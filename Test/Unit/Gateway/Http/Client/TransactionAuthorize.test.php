<?php declare(strict_types=1);

// TODO: Guzzle has MockHandler tools to mimc requests, but requires the Client to be passed
// to the class initializer so debug settings can be set. This can't be done with overloading.

use Msts\InvoiceMe\Gateway\Http\Client\TransactionAuthorize;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Msts\CaaS\ApiClient;

use Magento\Payment\Model\Method\Logger;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\ConfigProvider;
use Psr\Log\LoggerInterface;
use Msts\CaaS\Model\Data\Preauthorization\CreateMethod\CreateAPreauthorizationRequest;
use Msts\CaaS\Api\Data\Preauthorization\CreateMethod\CreateAPreauthorizationRequestInterfaceFactory;
use Msts\CaaS\CaaS;
use Msts\CaaS\Model\MaskValue;
use Magento\Framework\ObjectManagerInterface;
use Msts\CaaS\Model\Http\MstsRequest;
use Msts\CaaS\Model\Http\MstsRequestFactory;
use Msts\CaaS\Model\ClientConfigProvider;
use Msts\CaaS\CaaSOptions;
use Msts\CaaS\Model\Preauthorization\PreauthorizationApiCall;
use Msts\CaaS\Http\TransferBuilder;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class TransactionAuthorizeTest extends MockeryTestCase
{
    private $transactionAuthorize;
    private $loggerMock;
    private $paymentLoggerMock;
    private $preAuthorizationRequestFactoryMock;
    private $configProviderMock;
    private $caaSFactoryMock;
    private $preauthData;
    private $objectManagerMock;
    private $maskValueMock;
    private $mstsRequestFactory;
    private $clientConfigProvider;
    private $httpRequest;
    private $caasMock;
    private $preauthorizationApiCallMock;

  /** @Setup */
    protected function setUp(): void
    {
        $this->loggerMock = Mockery::mock(LoggerInterface::class);
        $this->paymentLoggerMock = Mockery::mock(Logger::class);
        $this->preAuthorizationRequestFactoryMock = Mockery::mock(CreateAPreauthorizationRequestInterfaceFactory::class);
        $this->configProviderMock = Mockery::mock(ConfigProvider::class);
        $this->caaSFactoryMock = Mockery::mock(CaaSFactory::class);
        $this->httpRequest = Mockery::mock(MstsRequest::class);
        $this->caasOptions = Mockery::mock(CaaSOptions::class);
        $this->caasMock = Mockery::mock(CaaS::class);
        $this->preauthorizationApiCallMock = Mockery::mock(PreauthorizationApiCall::class);
        $this->objectManagerMock = Mockery::mock(ObjectManagerInterface::class);
        $this->maskValueMock = Mockery::mock(MaskValue::class);
        $this->mstsRequestFactory = Mockery::mock(MstsRequestFactory::class);
        $this->clientConfigProvider = Mockery::mock(ClientConfigProvider::class);

        $this->assignMockValues();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

  /** @test */

    public function test_returns_correct_values()
    {
        $processFunction = $this->makeProcessPublic();
        $result = $processFunction->invokeArgs($this->transactionAuthorize, [$this->preauthData]);
        $this->assertEquals([
        "id" => "a42118dd-a93f-40a8-87af-56d4b63f6e78",
        "seller_id" => "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee",
        "buyer_id" => "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee",
        "po_number" => "test123",
        "currency" => "USD",
        "status" => "Preauthorized",
        "expires" => "2020-07-16T05:48:49.557Z",
        "created" => "2020-07-02T05:48:49.503Z",
        "modified" => "2020-07-02T05:48:49.503Z",
        "preauthorized_amount" => 100
        ], $result);
    }

    public function makeProcessPublic()
    {
        $reflection = new ReflectionClass($this->transactionAuthorize);
        $method = $reflection->getMethod('process');
        $method->setAccessible(true);
        return $method;
    }

  /** @helper functions */

    public function assignMockValues(): void
    {
        $this->preauthData = [
        'seller_id' => '1',
        'buyer_id' => '2',
        'currency' => 'AUD',
        'preauthorized_amount' => '10000',
        'po_number' => 'po123',
        ];

        $this->configProviderMock->allows([
        "getApiKey" => 'apikey123',
        "getApiUrl" => 'https://www.example.com',
        "getUri" => 'https://www.example.com',
        ]);

        $this->objectManagerMock->allows([
        "create" => new CaaSOptions(),
        "create" => $this->caasMock,
        ]);

        $this->caasMock->allows([
        'setLogger' => '',
        'setMaskValue' => '',
        'setRequestClass' => '',
        'preauthorization' => $this->preauthorizationApiCallMock
        ]);

        $mock = new MockHandler([
        new Response(201, [], json_encode([
        "id" => "a42118dd-a93f-40a8-87af-56d4b63f6e78",
        "seller_id" => "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee",
        "buyer_id" => "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee",
        "po_number" => "test123",
        "currency" => "USD",
        "status" => "Preauthorized",
        "expires" => "2020-07-16T05:48:49.557Z",
        "created" => "2020-07-02T05:48:49.503Z",
        "modified" => "2020-07-02T05:48:49.503Z",
        "preauthorized_amount" => 100
        ])),
        ]);

        $handlerStack = HandlerStack::create($mock);

        $this->caasMock->preauthorization = new PreauthorizationApiCall(
            new ApiClient($this->loggerMock, $this->maskValueMock, new Client(['handler' => $handlerStack])),
            new MstsRequest(
                new TransferBuilder(),
                $this->configProviderMock,
                $this->maskValueMock
            )
        );

        $this->preauthorizationApiCallMock->allows([
        'create' => ['test']
        ]);

        $this->clientConfigProvider->allows([
        "setBaseUri" => $this->clientConfigProvider,
        "getBaseUri" => 'https://www.example.com',
        "getUri" => 'https://www.example.com',
        ]);

        $this->mstsRequestFactory->allows([
        'create' => $this->httpRequest
        ]);

        $this->maskValueMock->allows([
        'mask' => $this->httpRequest
        ]);

        $this->maskValueMock->shouldReceive('mask')->andReturnUsing(function (string $value) {
            return $value;
        });

        $this->maskValueMock->shouldReceive('maskValues')->andReturnUsing(function (array $data, string $methodName) {
            return $data;
        });

        $this->loggerMock->allows([
        'debug' => null
        ]);

        $this->transactionAuthorize = new TransactionAuthorize(
            $this->loggerMock,
            $this->paymentLoggerMock,
            $this->preAuthorizationRequestFactoryMock,
            $this->configProviderMock,
            new CaaSFactory(
                $this->objectManagerMock,
                $this->loggerMock,
                $this->maskValueMock,
                $this->mstsRequestFactory,
                $this->configProviderMock,
                $this->clientConfigProvider,
                'CaaS::class'
            )
        );

        $this->caaSFactoryMock->allows([
        'create' => new CaaS('hello123'),
        "preauthorization" => [
        'create' => ['test' => 'test']
        ]
        ]);

        $this->preAuthorizationRequestFactoryMock->allows(["create" => new CreateAPreauthorizationRequest($this->preauthData)]);
    }
}
