<?php declare(strict_types=1);

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Msts\InvoiceMe\Gateway\Request\RefundBuilder;
use Msts\InvoiceMe\Model\CurrencyConverter;
use Msts\CaaS\Api\Data\Charge\ChargeDetailInterfaceFactory;
use Msts\CaaS\Api\Data\Charge\TaxDetailInterfaceFactory;
use Msts\CaaS\Model\Data\Charge\ChargeDetail;
use Msts\CaaS\Model\Data\Charge\TaxDetail;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Helper\Data;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\ResourceModel\Order\Tax\Item as TaxItem;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection as CreditMemoCollection;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory;
use Magento\Sales\Model\Order\Creditmemo\Item as CreditMemoItem;

class RefundBuilderTest extends MockeryTestCase
{
    private $subjectReaderMock;
    private $storeManagerMock;
    private $storeMock;
    private $currencyConverterMock;
    private $chargeDetailFactoryMock;
    private $taxDetailFactoryMock;
    private $taxDataMock;
    private $paymentMock;
    private $orderMock;
    private $orderItemMock;
    private $invoiceMock;
    private $invoiceCollectionMock;
    private $invoiceItemMock;
    private $creditMemoCollectionMock;
    private $creditMemoItemMock;
    private $taxItemMock;
    private $itemRepositoryMock;

  /** @Setup */
    protected function setUp(): void
    {
        $this->storeManagerMock = Mockery::mock(StoreManagerInterface::class);
        $this->subjectReaderMock = Mockery::mock(SubjectReader::class);
        $this->collectionFactoryMock = Mockery::mock(CollectionFactory::class);
        $this->creditMemoCollectionMock = Mockery::mock(CreditMemoCollection::class);
        $this->priceCurrencyMock = Mockery::mock(PriceCurrencyInterface::class);
        $this->paymentDataObjectMock = Mockery::mock(PaymentDataObjectInterface::class);
        $this->paymentMock = Mockery::mock(Payment::class);
        $this->storeMock = Mockery::mock(Store::class);
        $this->currencyConverterMock = Mockery::mock(CurrencyConverter::class);
        $this->chargeDetailFactoryMock = Mockery::mock(ChargeDetailInterfaceFactory::class);
        $this->taxDetailFactoryMock = Mockery::mock(TaxDetailInterfaceFactory::class);
        $this->taxDataMock = Mockery::mock(Data::class);
        $this->creditMemoMock = Mockery::mock(Creditmemo::class);
        $this->creditMemoItemMock = Mockery::mock(CreditMemoItem::class);

        $this->orderMock = Mockery::mock(Order::class);
        $this->orderItemMock = Mockery::mock(OrderItem::class);
        $this->invoiceMock = Mockery::mock(Invoice::class);
        $this->invoiceItemMock = Mockery::mock(InvoiceItem::class);
        $this->invoiceCollectionMock = Mockery::mock(InvoiceCollection::class);

        $this->taxItemMock = Mockery::mock(TaxItem::class);
        $this->itemRepositoryMock = Mockery::mock(OrderItemRepositoryInterface::class);

        $this->assignMockValues();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

  /** @test */
    public function test_full_refund_returns_correct_values()
    {
        $this->creditMemoMock->shouldReceive('getBaseGrandTotal')->andReturn(110)->byDefault();
        $this->invoiceCollectionMock->shouldReceive('getItems')->andReturn([$this->invoiceMock]);
        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);

        $this->assertEquals([
        'is_full_refund' => true
        ], $result);
    }

    public function test_partial_refund_returns_correct_values()
    {
        $this->invoiceCollectionMock->shouldReceive('getItems')->andReturn([$this->invoiceMock]);
        $this->subjectReaderMock->shouldReceive('readAmount')->andReturn(50)->byDefault();
        ;
        $this->creditMemoMock->shouldReceive('getBaseGrandTotal')->andReturn(50)->byDefault();
        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);

        $this->assertEquals(
            [
            'is_full_refund' => false,
            'return_amount' => 50,
            'total_amount' => 60,
            'tax_amount' => 2,
            'shipping_amount' => 10,
            'discount_amount' => 0,
            'shipping_discount_amount' => 0,
            'shipping_tax_amount' => 0,
            'shipping_tax_details' => null,
            'return_reason' => 'Other',
            'details' => [
            new ChargeDetail([
            'sku' => 'sku123',
            'description' => 'Potatoes',
            'quantity' => 1.0,
            'unit_price' => -90,
            'tax_amount' => -10,
            'discount_amount' => 0,
            'subtotal' => -100,
            'tax_details' => [
              new TaxDetail([
                'tax_type' => 'Item Tax',
                'tax_rate' => 10.0000,
                'tax_amount' => -10,
              ]),
            ],
            ]),
            ]
            ],
            $result
        );
    }

    public function test_payment_not_found()
    {
        $this->paymentDataObjectMock->shouldReceive('getPayment')->andReturn(null)->byDefault();
        ;
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage('Payment not found');

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

    public function test_credit_memo_not_found()
    {
        $this->paymentMock->shouldReceive('getCreditmemo')->andReturn(null)->byDefault();
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage('CreditMemo not found');

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

    public function test_invoice_not_found()
    {
        $this->creditMemoMock->shouldReceive('getInvoice')->andReturn(null)->byDefault();
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage('Invoice not found');

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

    public function test_invalid_amount_for_refund()
    {
        $this->subjectReaderMock->shouldReceive('readAmount')->andReturn(-5)->byDefault();
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage('Invalid amount for a refund.');

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

    public function test_refund_amount_cant_be_more_than_paid()
    {
        $this->subjectReaderMock->shouldReceive('readAmount')->andReturn(500)->byDefault();
        $this->paymentMock->shouldReceive('getBaseAmountPaid')->andReturn(220)->byDefault();
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage('Invalid amount for a refund. Refund amount can\'t be higher than the paid amount.');

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

    public function test_refund_shipping_greater_than_invoice_shipping()
    {
        $this->creditMemoMock->shouldReceive('getBaseShippingAmount')->andReturn(20)->byDefault();
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage("Refunded shipping '20' is higher than invoiced shipping '10'");

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

    public function test_refund_shipping_greater_than_captures_shipping()
    {
        $this->orderMock->shouldReceive('getOrigData')->andReturn(20)->byDefault(); // base_shipping_refunded
        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage("Refunded shipping '0' is higher than captured shipping '-10'");

        $result = $this->refundBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }

  /** @helper functions */

    public function assignMockValues(): void
    {
        $this->storeMock->allows(['getId' => 111, 'getBaseCurrencyCode' => 'AUD']);
        $this->currencyConverterMock->allows(['getMultiplier' => 1]);

        $this->chargeDetailFactoryMock->shouldReceive('create')->andReturnUsing(function () {
            return new ChargeDetail();
        });
        $this->taxDetailFactoryMock->shouldReceive('create')->andReturnUsing(function () {
            return new TaxDetail();
        });

        $this->storeManagerMock->allows([
        'getStore' => $this->storeMock,
        'getId' => 1,
        ]);
        $this->subjectReaderMock->allows([
        'readPayment' => $this->paymentDataObjectMock,
        ]);
        $this->subjectReaderMock->shouldReceive('readAmount')->andReturn(220)->byDefault();
        $this->paymentDataObjectMock->shouldReceive('getPayment')->andReturn($this->paymentMock)->byDefault();
        $this->paymentDataObjectMock->allows([
        'getOrder' => $this->orderMock
        ]);
        $this->paymentMock->shouldReceive('getCreditmemo')->andReturn($this->creditMemoMock)->byDefault();
        $this->paymentMock->shouldReceive('getBaseAmountPaid')->andReturn(220)->byDefault();
        $this->paymentMock->allows([
        'getOrder' => $this->orderMock,
        'getBaseShippingAmount' => 10,
        ]);

        $orderInvoiceValues = [
        'getOrderId' => 1,
        'getBaseTaxAmount' => 22,
        'getBaseShippingAmount' => 10,
        'getBaseTotalPaid' => 210,
        'getGrandTotal' => 210,
        'getBaseGrandTotal' => 210,
        'getBaseSubtotal' => 210,
        'getBaseCustomerBalanceAmount' => 0,
        'getBaseShippingTaxAmount' => 0,
        'getBaseShippingDiscountTaxCompensationAmnt' => 0,
        'getBaseShippingDiscountAmount' => 0,
        'getBaseDiscountTaxCompensationAmount' => 0,
        'getBaseDiscountAmount' => 0,
        'getBaseTotalRefunded' => 0,
        'getBaseShippingDiscountTaxCompensationAmount' => 0,
        'getBaseGiftCardsAmount' => 0,
        'getBaseCustomerBalanceAmount' => 0,
        'getBaseDiscountTaxCompensationAmount' => 0,
        'getGwBasePrice' => 0,
        'getGwBaseTaxAmount' => 0,
        'getGwCardBasePrice' => 0,
        'getGwCardBaseTaxAmount' => 0,
        'getGwBasePriceInvoiced' => 0,
        'getGwBaseTaxAmountInvoiced' => 0,
        'getGwCardBasePriceInvoiced' => 0,
        'getGwCardBaseTaxInvoiced' => 0,
        ];


        $this->orderMock->shouldReceive('getOrigData')->andReturn(0)->byDefault(); // base_shipping_refunded
        $this->orderMock->allows(array_merge($orderInvoiceValues, [
        'getStoreId' => 1,
        'getId' => 333,
        'getOrderIncrementId' => 333,
        'getInvoiceCollection' => $this->invoiceCollectionMock,
        'getCurrencyCode' => 'AUD',
        'getItems' => [$this->orderItemMock],
        'getAllItems' => [$this->orderItemMock],
        ]));

        $this->invoiceMock->allows(array_merge($orderInvoiceValues, [
        'getEntityId' => null,
        'getIncrementId' => 32,
        'getItemsCollection' => [$this->invoiceItemMock],
        'getAllItems' => []
        ]));

        $itemValues = [
        'getGwBasePriceInvoiced' => 0,
        'getGwBaseTaxAmountInvoiced' => 0,
        'getSku' => 'Sku123',
        'getName' => 'Potatoes',
        'getQty' => 2,
        'getQtyOrdered' => 2,
        'getProductType' => 'configurable',
        'getBaseRowTotal' => 180,
        'getBaseRowTotalInclTax' => 200,
        'getBasePrice' => 90,
        'getBaseTaxAmount' => 10,
        'getBaseDiscountTaxCompensationAmount' => 0,
        'getBaseShippingDiscountTaxCompensationAmnt' => 0,
        'getBaseSubtotal' => 200,
        'getHasChildren' => false,
        'getBaseDiscountAmount' => 0,
        'getBaseShippingTaxAmount' => 0,
        'getOrderItem' => $this->orderItemMock,
        'getOrderItemId' => 5,
        'getId' => 3,
        'getBaseDiscountTaxCompensationAmount' => 0,
        ];

        $this->invoiceItemMock->allows($itemValues);
        $this->orderItemMock->allows(array_merge($itemValues, [
        'getItemId' => 1,
        ]));

        $this->taxDataMock->allows([
        'priceIncludesTax' => true,
        'getCalculationSequence' => []
        ]);

        $this->creditMemoMock->shouldReceive('getInvoice')->andReturn($this->invoiceMock)->byDefault();
        $this->creditMemoMock->shouldReceive('getBaseShippingAmount')->andReturn(0)->byDefault();
        $this->creditMemoMock->allows([
        'getEntityId' => 222,
        'getItems' => [$this->creditMemoItemMock],
        'getAllItems' => [$this->creditMemoItemMock],
        'getOrder' => $this->orderMock,
        'getOrderId' => 333,
        'getBaseSubtotal' => 100,
        'getBaseGrandTotal' => 100,
        'getBaseTaxAmount' => 10,
        'getBaseShippingTaxAmount' => 0,
        'DiscountTaxCompensationAmnt' => 0,
        'getBaseShippingDiscountTaxCompensationAmnt' => 0,
        'getBaseDiscountTaxCompensationAmount' => 0,
        'getBaseDiscountAmount' => 0,
        'getGwBasePrice' => 0,
        'getGwBaseTaxAmount' => 0,
        'getGwCardBasePrice' => 0,
        'getGwCardBaseTaxAmount' => 0,
        'getBaseGiftCardsAmount' => 0,
        'getBaseCustomerBalanceAmount' => 0,
        'getAdjustmentNegative' => 0,
        'getAdjustmentPositive' => 0,
        ]);

        $this->creditMemoItemMock->allows([
        'getQty' => 1,
        'getOrderItemId' => 5,
        'getOrderItem' => $this->orderItemMock,
        'getBaseTaxAmount' => 10,
        'getBaseDiscountTaxCompensationAmount' => 0,
        'getBaseDiscountAmount' => 0,
        'getBaseRowTotal' => 90,
        'getBaseRowTotalInclTax' => 100,
        'getSku' => 'sku123',
        'getName' => 'Potatoes',
        'getBasePrice' => 90,
        ]);

        $this->collectionFactoryMock->allows([
        'create' => $this->creditMemoCollectionMock
        ]);

        $this->creditMemoCollectionMock->allows([
        'addFilter' => $this->creditMemoCollectionMock,
        'getIterator' => new \ArrayObject([$this->creditMemoMock]),
        'getEntityId' => 111,
        'count' => 1,
        ]);

        $this->priceCurrencyMock->shouldReceive('round')->andReturnUsing(function ($price) {
            return round($price, 2);
        });

        $this->invoiceMock->shouldReceive('getDataUsingMethod')->andReturnUsing(function ($key, $args = null) {
            $method = 'get' . str_replace('_', '', ucwords($key, '_'));
            return $this->invoiceMock->{$method}($args);
        });

        $this->invoiceItemMock->shouldReceive('getDataUsingMethod')->andReturnUsing(function ($key, $args = null) {
            $method = 'get' . str_replace('_', '', ucwords($key, '_'));
            return $this->invoiceItemMock->{$method}($args);
        });

        $this->taxItemMock->allows([
        'getTaxItemsByOrderId' => [
        [
          'tax_id' => '1',
          'tax_percent' => '10.0000',
          'item_id' => '1',
          'taxable_item_type' => 'product',
          'associated_item_id' => null,
          'real_amount' => '10.0000',
          'real_base_amount' => '10.0000',
          'code' => 'Item Tax',
          'title' => 'Item Tax',
          'order_id' => '333',
        ],
        ],
        ]);

        $this->refundBuilder = new RefundBuilder(
            $this->subjectReaderMock,
            $this->collectionFactoryMock,
            $this->storeManagerMock,
            $this->currencyConverterMock,
            $this->chargeDetailFactoryMock,
            $this->taxDetailFactoryMock,
            $this->priceCurrencyMock,
            $this->taxDataMock,
            $this->taxItemMock,
            $this->itemRepositoryMock
        );
    }
}
