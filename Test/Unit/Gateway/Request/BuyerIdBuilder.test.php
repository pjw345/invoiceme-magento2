<?php declare(strict_types=1);

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Msts\InvoiceMe\Model\GenerateGenericMessage;
use Msts\InvoiceMe\Registry\PaymentCapture;
use Msts\InvoiceMe\Gateway\Request\BuyerIdBuilder;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Framework\Phrase;

class BuyerIdBuilderTest extends MockeryTestCase
{
    private $paymentCaptureMock;
    private $subjectReaderMock;
    private $customerRegistryMock;
    private $encryptorMock;
    private $generateGenericMessageMock;
    private $buyerIdBuilder;
    private $paymentDataObjectMock;
    private $paymentMock;
    private $orderMock;
    private $customerMock;

  /** @Setup */
    protected function setUp(): void
    {
        $this->paymentCaptureMock = Mockery::mock(PaymentCapture::class);
        $this->subjectReaderMock = Mockery::mock(SubjectReader::class);
        $this->customerRegistryMock = Mockery::mock(CustomerRegistry::class);
        $this->encryptorMock = Mockery::mock(EncryptorInterface::class);
        $this->generateGenericMessageMock = Mockery::mock(GenerateGenericMessage::class);
        $this->paymentDataObjectMock = Mockery::mock(PaymentDataObjectInterface::class);
        $this->paymentMock = Mockery::mock(Payment::class);
        $this->orderMock = Mockery::mock(Order::class);
        $this->customerMock = Mockery::mock(Customer::class);

        $this->assignMockValues();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

  /** @test */
    public function test_returns_correct_values()
    {
        $result = $this->buildBuyerIdBuilder();
        $this->assertEquals(['buyer_id' => 1], $result);
    }

    public function test_skipped_payment_action_returns_empty_array()
    {
        $this->paymentCaptureMock->shouldReceive("isSkipped")->andReturn(true)->byDefault();
        $result = $this->buildBuyerIdBuilder();
        $this->assertEquals([], $result);
    }

    public function test_decrypter_fails_returns_exception()
    {
        $this->expectException(LocalizedException::class);

        $this->encryptorMock->shouldReceive("decrypt")->andReturn(null)->byDefault();
        $result = $this->buildBuyerIdBuilder();
        $this->assertEquals([], $result);
    }

  /** @helper functions */

    public function buildBuyerIdBuilder()
    {
        return $this->buyerIdBuilder->build(['payment' => $this->paymentDataObjectMock]);
    }
    public function assignMockValues(): void
    {
        $this->buyerIdBuilder = new BuyerIdBuilder(
            $this->paymentCaptureMock,
            $this->subjectReaderMock,
            $this->customerRegistryMock,
            $this->encryptorMock,
            $this->generateGenericMessageMock,
        );
        $this->subjectReaderMock->allows(["readPayment" => $this->paymentDataObjectMock]);
        $this->orderMock->allows(["getCustomerId" => 123]);
        $this->customerMock->allows(["getData" => 165278]);
        $this->generateGenericMessageMock->allows(["execute" => new Phrase('error')]);
        $this->paymentCaptureMock->shouldReceive("isSkipped")->andReturn(false)->byDefault();
        $this->encryptorMock->shouldReceive("decrypt")->andReturn(1)->byDefault();

        $this->paymentDataObjectMock->allows(["getPayment" => $this->paymentMock, "getOrder" => $this->orderMock]);
        $this->customerRegistryMock->allows(["retrieve" => $this->customerMock]);
    }
}
