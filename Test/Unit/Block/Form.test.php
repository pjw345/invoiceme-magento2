<?php declare(strict_types=1);

use Msts\InvoiceMe\Block\Form;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResourceModel;
use Magento\Backend\Model\Session\Quote as QuoteSession;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Eav\Model\Entity\AbstractEntity;
use Magento\Quote\Model\Quote;
use Magento\Framework\View\Element\Template\Context as ViewContext;

class FormTest extends MockeryTestCase
{
    private $form;
    private $customerResourceMock;
    private $quoteSessionMock;
    private $quoteMock;
    private $mstsImStatusMock;
    private $customerMock;
    private $viewContextMock;

  /** @Setup */
    protected function setUp(): void
    {
        $this->configProviderMock = Mockery::mock(ConfigProvider::class);
        $this->customerResourceMock = Mockery::mock(CustomerResourceModel::class);
        $this->quoteSessionMock = Mockery::mock(QuoteSession::class);
        $this->quoteMock = Mockery::mock(Quote::class);
        $this->customerMock = Mockery::mock(CustomerInterface::class);
        $this->mstsImStatusMock = Mockery::mock(AbstractEntity::class);
        $this->viewContextMock = Mockery::mock(ViewContext::class);

        $this->assignMockValues();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

  /** @test */
    public function test_getValidationMessage_not_registered()
    {
        $this->mstsImStatusMock->shouldReceive('usesSource')->andReturn(null)->byDefault();
        $result = $this->form->getValidationMessage();
        $this->assertEquals('InvoiceMe payment method is currently not available to this customer as the customer is not registered in MSTS yet.', $result);
    }

    public function test_getValidationMessage_not_active()
    {
        $this->mstsImStatusMock->shouldReceive('getOptionText')->andReturn('Declined')->byDefault();
        $result = $this->form->getValidationMessage();
        $this->assertEquals('InvoiceMe payment method is currently not available to this customer as their MSTS status is "Declined".', $result);
    }

    public function test_getValidationMessage_active_returns_empty_string()
    {
        $this->mstsImStatusMock->shouldReceive('getOptionText')->andReturn('Active')->byDefault();
        $result = $this->form->getValidationMessage();
        $this->assertEquals('', $result);
    }

    public function test_isCustomerActiveBuyer_true()
    {
        $this->mstsImStatusMock->shouldReceive('getOptionText')->andReturn('Active')->byDefault();
        $result = $this->form->isCustomerActiveBuyer();
        $this->assertEquals(true, $result);
    }

    public function test_isCustomerActiveBuyer_false()
    {
        $this->mstsImStatusMock->shouldReceive('getOptionText')->andReturn('Declined')->byDefault();
        $result = $this->form->isCustomerActiveBuyer();
        $this->assertEquals(false, $result);
    }

  /** @helper functions */

    public function assignMockValues(): void
    {
        $this->customerResourceMock->shouldReceive('getAttribute')->andReturn($this->mstsImStatusMock)->byDefault();
        $this->quoteSessionMock->shouldReceive('getQuote')->andReturn($this->quoteMock)->byDefault();
        $this->quoteMock->shouldReceive('getCustomer')->andReturn($this->customerMock)->byDefault();
        $this->customerMock->shouldReceive('getCustomAttribute')->andReturn($this->mstsImStatusMock)->byDefault();
        $this->customerMock->shouldReceive('statusAttribute')->andReturn('Active')->byDefault();
        $this->mstsImStatusMock->shouldReceive('getValue')->andReturn(true)->byDefault();
        $this->mstsImStatusMock->shouldReceive('getOptionText')->andReturn('Active')->byDefault();
        $this->mstsImStatusMock->shouldReceive('usesSource')->andReturn(true)->byDefault();
        $this->mstsImStatusMock->allows(["getSource" => $this->mstsImStatusMock]);
        $this->viewContextMock->allows([
        'getValidator' => '',
        'getResolver' => '',
        'getFilesystem' => '',
        'getEnginePool' => '',
        'getStoreManager' => '',
        'getAppState' => '',
        'getPageConfig' => '',
        'getRequest' => '',
        'getLayout' => '',
        'getEventManager' => '',
        'getUrlBuilder' => '',
        'getCache' => '',
        'getDesignPackage' => '',
        'getSession' => '',
        'getSidResolver' => '',
        'getScopeConfig' => '',
        'getAssetRepository' => '',
        'getViewConfig' => '',
        'getCacheState' => '',
        'getLogger' => '',
        'getEscaper' => '',
        'getFilterManager' => '',
        'getLocaleDate' => '',
        'getInlineTranslation' => '',
        'getLockGuardedCacheLoader' => '',
        ]);

        $this->form = new Form($this->quoteSessionMock, $this->customerResourceMock, $this->viewContextMock, []);
    }
}
