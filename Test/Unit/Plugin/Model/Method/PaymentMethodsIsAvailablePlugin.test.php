<?php declare(strict_types=1);

use Msts\InvoiceMe\Plugin\Model\Method\PaymentMethodIsAvailablePlugin;
use Mockery\Adapter\Phpunit\MockeryTestCase;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Model\Customer\Data;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Api\Data\CartInterface;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Customer\GetCustomerStatus;
use Msts\InvoiceMe\Model\IsModuleFullyConfigured;
use Msts\InvoiceMe\Model\OptionSource\Availability;


class PaymentMethodIsAvailablePluginTest extends MockeryTestCase
{
    private $subjectMock;
    private $proceedClosureMock;
    private $paymentMethodIsAvailablePlugin;
    private $isModuleFullyConfiguredMock;
    private $configProviderMock;
    private $customerSessionMock;
    private $customerRegistryMock;
    private $customerRegistryCustomerMock;
    private $getCustomerStatusMock;
    private $quoteMock;
    private $customerMock;
    private $customerDataMock;

  /** @Setup */
    protected function setUp(): void
    {
        $this->subjectMock = Mockery::mock(MethodInterface::class);
        $this->isModuleFullyConfiguredMock = Mockery::mock(IsModuleFullyConfigured::class);
        $this->configProviderMock = Mockery::mock(ConfigProvider::class);
        $this->customerSessionMock = Mockery::mock(Session::class);
        $this->customerRegistryMock = Mockery::mock(CustomerRegistry::class);
        $this->customerRegistryCustomerMock = Mockery::mock(Customer::class);
        $this->getCustomerStatusMock = Mockery::mock(GetCustomerStatus::class);
        $this->customerMock = Mockery::mock(Customer::class);
        $this->customerDataMock = Mockery::mock(Data::class);
        $this->quoteMock = Mockery::mock(CartInterface::class);

        $this->proceedClosureMock = function () {
            return true;
        };

        $this->assignMockValues();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

  /** @test */
    public function test_returns_correct_values()
    {
        $this->isModuleFullyConfiguredMock->shouldReceive('execute')->andReturn(true);
        $this->configProviderMock->shouldReceive('getAvailabilityForCustomers')->andReturn(Availability::ALL_CUSTOMERS);
        $this->customerMock->shouldReceive('getId')->andReturn(12345);

        $result = $this->paymentMethodIsAvailablePlugin->aroundIsAvailable(
            $this->subjectMock,
            $this->proceedClosureMock,
            null
        );

        $this->assertEquals(true, $result);
    }

    public function test_returns_false_when_not_fully_configured()
    {
        $this->isModuleFullyConfiguredMock->shouldReceive('execute')->andReturn(false);

        $result = $this->paymentMethodIsAvailablePlugin->aroundIsAvailable(
            $this->subjectMock,
            $this->proceedClosureMock,
            $this->quoteMock
        );

        $this->assertEquals(false, $result);
    }

    public function test_returns_false_when_customer_id_not_present_and_quote_is_null()
    {
        $this->isModuleFullyConfiguredMock->shouldReceive('execute')->andReturn(true);
        $this->configProviderMock->shouldReceive('getAvailabilityForCustomers')->andReturn(Availability::ACTIVE_BUYERS_ONLY);
        $this->customerMock->shouldReceive('getId')->andReturn(null);

        $result = $this->paymentMethodIsAvailablePlugin->aroundIsAvailable(
            $this->subjectMock,
            $this->proceedClosureMock,
            null
        );

        $this->assertEquals(false, $result);
    }

    public function test_returns_false_when_customer_id_not_present_and_quote_customer_id_not_present()
    {
        $this->isModuleFullyConfiguredMock->shouldReceive('execute')->andReturn(true);
        $this->configProviderMock->shouldReceive('getAvailabilityForCustomers')->andReturn(Availability::ACTIVE_BUYERS_ONLY);
        $this->customerMock->shouldReceive('getId')->andReturn(null);
        $this->customerDataMock->shouldReceive('getId')->andReturn(null);
        $this->quoteMock->shouldReceive('getCustomer')->andReturn($this->customerDataMock);

        $result = $this->paymentMethodIsAvailablePlugin->aroundIsAvailable(
            $this->subjectMock,
            $this->proceedClosureMock,
            $this->quoteMock
        );

        $this->assertEquals(false, $result);
    }

    public function test_returns_false_when_customer_id_not_present_and_quote_customer_id_is_present()
    {
        $this->isModuleFullyConfiguredMock->shouldReceive('execute')->andReturn(true);
        $this->configProviderMock->shouldReceive('getAvailabilityForCustomers')->andReturn(Availability::ACTIVE_BUYERS_ONLY);
        $this->customerMock->shouldReceive('getId')->andReturn(null);
        $this->customerDataMock->shouldReceive('getId')->andReturn(12345);
        $this->quoteMock->shouldReceive('getCustomer')->andReturn($this->customerDataMock);
        $this->customerRegistryCustomerMock->shouldReceive('getId')->andReturn(12345);
        $this->customerRegistryMock->shouldReceive('retrieve')->andReturn($this->customerRegistryCustomerMock);

        $result = $this->paymentMethodIsAvailablePlugin->aroundIsAvailable(
            $this->subjectMock,
            $this->proceedClosureMock,
            $this->quoteMock
        );

        $this->assertEquals(true, $result);
    }

  /** @helper functions */

    public function assignMockValues(): void
    {
        $this->paymentMethodIsAvailablePlugin = new PaymentMethodIsAvailablePlugin(
            $this->isModuleFullyConfiguredMock,
            $this->configProviderMock,
            $this->customerSessionMock,
            $this->customerRegistryMock,
            $this->getCustomerStatusMock
        );

        $this->subjectMock->shouldReceive('getCode')->andReturn(ConfigProvider::CODE);

        $this->customerSessionMock->shouldReceive('getCustomer')->andReturn($this->customerMock);
        $this->getCustomerStatusMock->shouldReceive('execute')->andReturn(BuyerStatusInterface::ACTIVE);
    }
}
