<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Plugin;

use Magento\Quote\Model\Quote\Payment;

class AdditionalInformation
{
    private const INVOICEME_PO_NUMBER = 'invoiceme_po_number';
    private const INVOICEME_NOTES = 'invoiceme_notes';

    /**
     * @var array
     */
    protected $additionalKeys = [
        self::INVOICEME_PO_NUMBER,
        self::INVOICEME_NOTES,
    ];

    /**
     * @param Payment $subject
     * @param array|mixed|null $result
     * @return array|mixed|null
     */
    public function afterGetAdditionalInformation(Payment $subject, $result)
    {
        if (is_array($result)) {
            foreach ($this->additionalKeys as $additionalKey) {
                if (!array_key_exists($additionalKey, $result) && $subject->hasData($additionalKey)) {
                    $result[$additionalKey] = $subject->getDataUsingMethod($additionalKey);
                }
            }
        }

        return $result;
    }
}
