<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Plugin\Model\Sales\Order\Payment\Operations;

use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order\Payment\Operations\AuthorizeOperation;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Customer\IsRegisteredMstsBuyer;
use Psr\Log\LoggerInterface;

class AuthorizeOperationPlugin
{
    /**
     * @var State
     */
    private $appState;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var IsRegisteredMstsBuyer
     */
    private $isRegisteredMstsBuyer;

    public function __construct(
        State $appState,
        Request $request,
        CustomerRepositoryInterface $customerRepository,
        LoggerInterface $logger,
        IsRegisteredMstsBuyer $isRegisteredMstsBuyer
    ) {
        $this->appState = $appState;
        $this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->isRegisteredMstsBuyer = $isRegisteredMstsBuyer;
    }

    /**
     * Skip creating payment authorization if the customer is not a registered MSTS buyer yet
     *
     * @param AuthorizeOperation $subject
     * @param OrderPaymentInterface $payment
     * @param bool $isOnline
     * @param string|float $amount
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeAuthorize(
        AuthorizeOperation $subject,
        OrderPaymentInterface $payment,
        bool $isOnline,
        $amount
    ): ?array {
        try {
            if ($this->appState->getAreaCode() === Area::AREA_WEBAPI_REST) {
                $requestData = $this->request->getRequestData();
                if (!isset($requestData['requestSource']) || $requestData['requestSource'] !== 'frontend_checkout') {
                    return null;
                }
            }
        } catch (LocalizedException $e) {
            return null;
        }

        if ($payment->getMethod() !== ConfigProvider::CODE) {
            return null;
        }

        $customerId = $payment->getOrder()->getCustomerId();
        if (!$customerId) {
            return null;
        }

        try {
            $customer = $this->customerRepository->getById($customerId);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);

            return null;
        }

        if (!$this->isRegisteredMstsBuyer->execute($customer)) {
            $isOnline = false;
            $payment->setIsTransactionPending(true);
        }

        return [$payment, $isOnline, $amount];
    }
}
