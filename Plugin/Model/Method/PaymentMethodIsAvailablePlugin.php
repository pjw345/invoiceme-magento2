<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Plugin\Model\Method;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Api\Data\CartInterface;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Customer\GetCustomerStatus;
use Msts\InvoiceMe\Model\IsModuleFullyConfigured;
use Msts\InvoiceMe\Model\OptionSource\Availability;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class PaymentMethodIsAvailablePlugin
{
    /**
     * @var IsModuleFullyConfigured
     */
    private $isModuleFullyConfigured;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var CustomerRegistry
     */
    private $customerRegistry;

    /**
     * @var GetCustomerStatus
     */
    private $getCustomerStatus;

    public function __construct(
        IsModuleFullyConfigured $isModuleFullyConfigured,
        ConfigProvider $configProvider,
        Session $customerSession,
        CustomerRegistry $customerRegistry,
        GetCustomerStatus $getCustomerStatus
    ) {
        $this->isModuleFullyConfigured = $isModuleFullyConfigured;
        $this->configProvider = $configProvider;
        $this->customerSession = $customerSession;
        $this->customerRegistry = $customerRegistry;
        $this->getCustomerStatus = $getCustomerStatus;
    }

    /**
     * @param MethodInterface $subject
     * @param \Closure $proceed
     * @param CartInterface $quote
     * @return bool
     */
    public function aroundIsAvailable(
        MethodInterface $subject,
        \Closure $proceed,
        ?CartInterface $quote = null
    ): bool {
        if ($subject->getCode() === ConfigProvider::CODE
            && (!$this->isModuleFullyConfigured->execute()
                || !$this->canCustomerUsePaymentMethod($quote))) {
            return false;
        }

        return $proceed($quote);
    }

    private function canCustomerUsePaymentMethod(?CartInterface $quote = null): bool
    {
        if ($this->configProvider->getAvailabilityForCustomers() === Availability::ALL_CUSTOMERS) {
            return true;
        }

        $customer = $this->customerSession->getCustomer();

        if (!$customer->getId() && $quote) {
            $customerId = $quote->getCustomer()->getId();

            if ($customerId) {
                $customer = $this->customerRegistry->retrieve($customerId);
            }
        }

        if (!$customer->getId()) {
            return false;
        }

        return $this->getCustomerStatus->execute($customer) === BuyerStatusInterface::ACTIVE;
    }
}
