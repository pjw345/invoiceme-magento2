<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Plugin\Model\Customer;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Customer\DataProviderWithDefaultAddresses;
use Magento\Framework\Encryption\EncryptorInterface;
use Msts\InvoiceMe\Model\PriceFormatter;

class DataProviderWithDefaultAddressesPlugin
{
    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    public function __construct(EncryptorInterface $encryptor, PriceFormatter $priceFormatter)
    {
        $this->encryptor = $encryptor;
        $this->priceFormatter = $priceFormatter;
    }

    /**
     * Prepare values of the MSTS InvoiceMe related customer attributes for the InvoiceMe section
     * at the Customer Edit Page in the Magento Admin Panel
     *
     * @see \Magento\Customer\Model\Customer\DataProviderWithDefaultAddresses::getData()
     *
     * @param DataProviderWithDefaultAddresses $subject
     * @param array $result
     * @return array
     */
    public function afterGetData(DataProviderWithDefaultAddresses $subject, array $result): array
    {
        $items = $subject->getCollection()->getItems();
        if (empty($items)) {
            return $result;
        }

        $customers = [];
        /** @var Customer $customer */
        foreach ($items as $customer) {
            $customers[$customer->getId()] = $customer;
        }

        foreach ($result as $customerId => $customerData) {
            if (!isset($customerData['customer']) || !is_array($customerData['customer'])) {
                continue;
            }

            $data = $customerData['customer'];
            $currentCustomer = $customers[$customerId];

            $result[$customerId]['customer'] = $this->prepareInvoiceMeCustomerData($data, $currentCustomer);
        }

        return $result;
    }

    private function prepareInvoiceMeCustomerData(array $data, Customer $customer): array
    {
        if (!empty($data['msts_im_buyer_id'])) {
            $buyerId = $this->encryptor->decrypt($data['msts_im_buyer_id']);
            $data['msts_im_buyer_id'] = substr($buyerId, 0, 8);
        }

        if (!empty($data['msts_im_status'])) {
            $data['msts_im_status'] = $customer->getAttribute('msts_im_status')
                ->getFrontend()
                ->getValue($customer);
        }

        $currency = '';
        if (!empty($data['msts_im_currency'])) {
            $currency = $data['msts_im_currency'];
        }

        if (isset($data['msts_im_credit_approved'])) {
            $data['msts_im_credit_approved'] = $this->priceFormatter->getPriceFormattedInEasyToCopyFormat(
                (float) $data['msts_im_credit_approved'],
                $currency
            );
        }

        if (isset($data['msts_im_credit_balance'])) {
            $data['msts_im_credit_balance'] = $this->priceFormatter->getPriceFormattedInEasyToCopyFormat(
                (float) $data['msts_im_credit_balance'],
                $currency
            );
        }

        if (isset($data['msts_im_credit_preauthorized'])) {
            $data['msts_im_credit_preauthorized'] = $this->priceFormatter->getPriceFormattedInEasyToCopyFormat(
                (float) $data['msts_im_credit_preauthorized'],
                $currency
            );
        }

        return $data;
    }
}
