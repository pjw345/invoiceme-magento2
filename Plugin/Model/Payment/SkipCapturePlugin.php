<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Plugin\Model\Payment;

use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Payment\Model\MethodInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Customer\IsRegisteredMstsBuyer;
use Msts\InvoiceMe\Registry\PaymentCapture;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SkipCapturePlugin
{
    /**
     * @var State
     */
    private $appState;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var IsRegisteredMstsBuyer
     */
    private $isRegisteredMstsBuyer;

    /**
     * @var PaymentCapture
     */
    private $paymentCapture;

    public function __construct(
        State $appState,
        Request $request,
        CustomerRepositoryInterface $customerRepository,
        LoggerInterface $logger,
        IsRegisteredMstsBuyer $isRegisteredMstsBuyer,
        PaymentCapture $paymentCapture
    ) {
        $this->appState = $appState;
        $this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->isRegisteredMstsBuyer = $isRegisteredMstsBuyer;
        $this->paymentCapture = $paymentCapture;
    }

    /**
     * Skip creating payment capture if the customer is not a registered MSTS buyer yet
     *
     * @param MethodInterface $subject
     * @param OrderPaymentInterface $payment
     * @param float $amount
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeCapture(
        MethodInterface $subject,
        OrderPaymentInterface $payment,
        float $amount
    ): ?array {
        try {
            if ($this->appState->getAreaCode() === Area::AREA_WEBAPI_REST) {
                $requestData = $this->request->getRequestData();
                if (!isset($requestData['requestSource']) || $requestData['requestSource'] !== 'frontend_checkout') {
                    return null;
                }
            }
        } catch (LocalizedException $e) {
            return null;
        }

        if ($payment->getMethod() !== ConfigProvider::CODE) {
            return null;
        }

        $customerId = $payment->getOrder()->getCustomerId();
        if (!$customerId) {
            return null;
        }

        try {
            $customer = $this->customerRepository->getById($customerId);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);

            return null;
        }

        if (!$this->isRegisteredMstsBuyer->execute($customer)) {
            $this->paymentCapture->skip();
            $payment->setIsTransactionPending(true);
        }

        return [$payment, $amount];
    }
}
