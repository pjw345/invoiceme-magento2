<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Sales\Model\Order;
use Msts\InvoiceMe\Model\ConfigProvider;

class CanCreateCreditMemoPlugin
{
    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    public function afterCanCreditmemo(Order $subject, bool $result): bool
    {
        if (!$result || $subject->getPayment()->getMethod() !== ConfigProvider::CODE) {
            return $result;
        }

        return $this->request->getFullActionName() !== 'sales_order_view';
    }
}
