<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Observer;

use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Msts\InvoiceMe\Exception\NotUniqueClientReferenceIdException;
use Msts\InvoiceMe\Model\ResourceModel\IsExistsClientReferenceId;

class ValidateClientReferenceIdUniquenessObserver implements ObserverInterface
{
    /**
     * @var IsExistsClientReferenceId
     */
    private $isExistsClientReferenceId;

    public function __construct(
        IsExistsClientReferenceId $isExistsClientReferenceId
    ) {
        $this->isExistsClientReferenceId = $isExistsClientReferenceId;
    }

    /**
     * @param Observer $observer
     * @throws NotUniqueClientReferenceIdException
     * @throws LocalizedException
     */
    public function execute(Observer $observer): void
    {
        /** @var Customer $customer */
        $customer = $observer->getData('customer');

        if (!$customer->getMstsImClientReferenceId()) {
            return;
        }

        $isExistsClientReferenceId = $this->isExistsClientReferenceId->execute(
            $customer->getDataModel(),
            $customer->getMstsImClientReferenceId()
        );
        if ($isExistsClientReferenceId) {
            throw new NotUniqueClientReferenceIdException(
                __('Could not assign a unique Client Reference ID to the Customer.')
            );
        }
    }
}
