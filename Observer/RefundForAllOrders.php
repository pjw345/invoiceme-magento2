<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Sales\Model\Order;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Model\RefundOrder;

class RefundForAllOrders implements ObserverInterface
{
    /**
     * @var RefundOrder
     */
    protected $refundOrder;

    /**
     * @param RefundOrder $refundOrder
     */
    public function __construct(RefundOrder $refundOrder)
    {
        $this->refundOrder = $refundOrder;
    }

    /**
     * Refund preauthorized amounts for all orders
     *
     * @param Observer $observer
     * @return $this
     * @throws ClientException
     * @throws ApiClientException
     */
    public function execute(Observer $observer)
    {
        $orders = $observer->getEvent()->getOrders();

        /** @var Order $order */
        foreach ($orders as $order) {
            $this->refundOrder->execute($order);
        }

        return $this;
    }
}
