<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;

class PaymentAdditionalDataAssignObserver extends AbstractDataAssignObserver
{
    private const INVOICEME_PO_NUMBER = 'invoiceme_po_number';
    private const INVOICEME_NOTES = 'invoiceme_notes';

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);

        $paymentInfo = $this->readPaymentModelArgument($observer);
        if (isset($additionalData[self::INVOICEME_PO_NUMBER])) {
            $paymentInfo->setAdditionalInformation(
                self::INVOICEME_PO_NUMBER,
                $additionalData[self::INVOICEME_PO_NUMBER]
            );
        }

        if (isset($additionalData[self::INVOICEME_NOTES])) {
            $paymentInfo->setAdditionalInformation(
                self::INVOICEME_NOTES,
                $additionalData[self::INVOICEME_NOTES]
            );
        }
    }
}
