<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Api\Data\Webhook;

interface EventTypeInterface
{
    public const BUYER_STATUS = 'buyer.status';

    public const PRE_AUTHORIZATION_UPDATED = 'preauthorization.updated';
}
