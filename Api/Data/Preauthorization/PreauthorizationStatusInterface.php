<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Api\Data\Preauthorization;

interface PreauthorizationStatusInterface
{
    public const CANCELLED = 'Cancelled';

    public const PREAUTHORIZED = 'Preauthorized';
}
