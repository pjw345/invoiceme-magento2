<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Api\Data\Buyer;

interface BuyerStatusInterface
{
    public const ACTIVE = 'Active';

    public const CANCELLED = 'Cancelled';

    public const DECLINED = 'Declined';

    public const INACTIVE = 'Inactive';

    public const PENDING = 'Pending';

    public const PENDING_DIRECT_DEBIT = 'Pending Direct Debit';

    public const PENDING_SETUP = 'Pending Setup';

    public const SUSPENDED = 'Suspended';

    public const WITHDRAWN = 'Withdrawn';
}
