<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Api\Data\Charge;

interface ReturnReasonInterface
{
    public const DELIVERY_REFUSED = 'Delivery Refused';

    public const MERCHANDISE_DAMAGED = 'Merchandise Damaged';

    public const MERCHANDISE_DEFECTIVE = 'Merchandise Defective';

    public const DUPLICATE_SHIPMENT = 'Duplicate Shipment';

    public const DUPLICATE_CONSIGNMENT = 'Duplicate Consignment';

    public const OTHER = 'Other';
}
