<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Api\Data\Charge;

interface ResponseStatusInterface
{
    public const CANCELLED = 'Cancelled';

    public const CREATED = 'Created';
}
