<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Http;

use GuzzleHttp\ClientInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Msts\CaaS\ApiClient;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Exception\ResponseException;
use Msts\CaaS\Http\Transfer;
use Msts\CaaS\Model\MaskValue;
use Msts\InvoiceMe\Model\GenerateGenericMessage;
use Psr\Log\LoggerInterface;

class ApiClientDecorator extends ApiClient
{
    /**
     * @var GenerateGenericMessage
     */
    private $generateGenericMessage;

    public function __construct(
        LoggerInterface $caaSLogger,
        MaskValue $maskValue,
        ClientInterface $client,
        GenerateGenericMessage $generateGenericMessage
    ) {
        parent::__construct(
            $caaSLogger,
            $maskValue,
            $client
        );
        $this->generateGenericMessage = $generateGenericMessage;
    }

    /**
     * @param Transfer $transfer
     * @return array
     * @throws ApiClientException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws ResponseException
     */
    public function execute(Transfer $transfer): array
    {
        try {
            return parent::execute($transfer);
        } catch (ResponseException $responseException) {
            throw new ResponseException(
                (string)$this->generateGenericMessage->execute(),
                $responseException->getErrorResponse(),
                $responseException,
                $responseException->getCode()
            );
        } catch (ApiClientException $exception) {
            throw $exception;
        }
    }
}
