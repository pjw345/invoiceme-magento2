<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model;

use Magento\Payment\Gateway\Http\ClientException;
use Magento\Sales\Model\Order;
use Msts\CaaS\Api\Data\Charge\CancelMethod\CancelAChargeRequestInterface;
use Msts\CaaS\Api\Data\Charge\CancelMethod\CancelAChargeRequestInterfaceFactory;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Api\Data\Charge\ResponseStatusInterface;
use Msts\InvoiceMe\Api\Data\Charge\ReturnReasonInterface;

class RefundOrder
{
    /**
     * @var CancelAChargeRequestInterfaceFactory
     */
    private $cancelAChargeRequestFactory;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    /**
     * @param CancelAChargeRequestInterfaceFactory $cancelAChargeRequestFactory
     * @param CaaSFactory $caaSFactory
     */
    public function __construct(
        CancelAChargeRequestInterfaceFactory $cancelAChargeRequestFactory,
        CaaSFactory $caaSFactory
    ) {
        $this->cancelAChargeRequestFactory = $cancelAChargeRequestFactory;
        $this->caaSFactory = $caaSFactory;
    }

    /**
     * @param Order $order
     * @throws ClientException
     * @throws ApiClientException
     */
    public function execute(Order $order): void
    {
        /** @var CancelAChargeRequestInterface $cancelAChargeRequest */
        $cancelAChargeRequest = $this->cancelAChargeRequestFactory->create();
        $cancelAChargeRequest->setId($order->getPayment()->getLastTransId());
        $cancelAChargeRequest->setReason(ReturnReasonInterface::OTHER);
        $caaS = $this->caaSFactory->create();
        $processCancelACharge = $caaS->charge->cancel($cancelAChargeRequest->getRequestData());
        if ($processCancelACharge->getStatus() !== ResponseStatusInterface::CANCELLED) {
            throw new ClientException(__('Payment refund error.'));
        }
    }
}
