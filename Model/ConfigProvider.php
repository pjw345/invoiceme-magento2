<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Msts\CaaS\Api\ConfigProviderInterface;
use Msts\CaaS\ApiClient;
use Msts\CaaS\Model\MaskValue;

class ConfigProvider implements ConfigProviderInterface
{
    public const CODE = 'msts_invoiceme';

    public const ORDER_STATUS_PENDING_INVOICEME = 'pending_invoiceme';

    private const ACTIVE = 'payment/msts_invoiceme/active';

    private const PAYMENT_ACTION = 'payment/msts_invoiceme/payment_action';

    private const IS_SANDBOX = 'payment/msts_invoiceme/sandbox';

    private const API_KEY = 'payment/msts_invoiceme/api_key';

    private const SELLER_ID = 'payment/msts_invoiceme/seller_id';

    private const API_URL = 'payment/msts_invoiceme/api_url';

    private const PROGRAM_URL = 'payment/msts_invoiceme/program_url';

    private const SANDBOX_API_KEY = 'payment/msts_invoiceme/sandbox_api_key';

    private const SANDBOX_SELLER_ID = 'payment/msts_invoiceme/sandbox_seller_id';

    private const SANDBOX_SELLER_URL = 'payment/msts_invoiceme/sandbox_api_url';

    private const SANDBOX_PROGRAM_URL = 'payment/msts_invoiceme/sandbox_program_url';

    private const IS_DEBUG_MODE = 'payment/msts_invoiceme/debug';

    private const NEW_ORDER_STATUS = 'payment/msts_invoiceme/order_status';

    private const AVAILABILITY_FOR_CUSTOMERS = 'payment/msts_invoiceme/availability';

    public const CREATED_WEBHOOKS = 'payment/msts_invoiceme/created_webhooks';

    public const API_KEY_FOR_CREATED_WEBHOOKS = 'payment/msts_invoiceme/api_key_for_created_webhooks';

    public const BASE_URL_FOR_CREATED_WEBHOOKS = 'payment/msts_invoiceme/base_url_for_created_webhooks';

    public const WEBHOOK_AUTH_TOKEN_FOR_CREATED_WEBHOOKS =
        'payment/msts_invoiceme/webhook_auth_token_for_created_webhooks';

    private const WEBHOOK_AUTH_TOKEN_HEADER_NAME = 'payment/msts_invoiceme/webhook_auth_token_header_name';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var MaskValue
     */
    private $maskValue;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Json $serializer,
        RequestInterface $request,
        MaskValue $maskValue
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->serializer = $serializer;
        $this->request = $request;
        $this->maskValue = $maskValue;
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return bool
     */
    public function isActive(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): bool
    {
        return $this->scopeConfig->isSetFlag(self::ACTIVE, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getPaymentAction(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue(self::PAYMENT_ACTION, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getApiUrl(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->isSandboxMode($scope, $scopeCode)
            ? $this->scopeConfig->getValue(self::SANDBOX_SELLER_URL, $scope, $scopeCode)
            : $this->scopeConfig->getValue(self::API_URL, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return bool
     */
    public function isSandboxMode(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): bool
    {
        return $this->scopeConfig->isSetFlag(self::IS_SANDBOX, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getApiKey(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->isSandboxMode($scope, $scopeCode)
                ? $this->scopeConfig->getValue(self::SANDBOX_API_KEY, $scope, $scopeCode)
                : $this->scopeConfig->getValue(self::API_KEY, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getSellerId(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->isSandboxMode($scope, $scopeCode)
                ? $this->scopeConfig->getValue(self::SANDBOX_SELLER_ID, $scope, $scopeCode)
                : $this->scopeConfig->getValue(self::SELLER_ID, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getProgramUrl(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->isSandboxMode($scope, $scopeCode)
            ? $this->scopeConfig->getValue(self::SANDBOX_PROGRAM_URL, $scope, $scopeCode)
            : $this->scopeConfig->getValue(self::PROGRAM_URL, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return array|null
     */
    public function getCreatedWebhooks(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?array
    {
        $value = $this->scopeConfig->getValue(self::CREATED_WEBHOOKS, $scope, $scopeCode);

        if ($value) {
            return $this->serializer->unserialize($value);
        }

        return null;
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return bool|null
     */
    public function isInDebugMode(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?bool
    {
        return $this->scopeConfig->isSetFlag(self::IS_DEBUG_MODE, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string
     */
    public function getAvailabilityForCustomers(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): string
    {
        return $this->scopeConfig->getValue(self::AVAILABILITY_FOR_CUSTOMERS, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getNewOrderStatus(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue(self::NEW_ORDER_STATUS, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getApiKeyForCreatedWebhooks(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue(self::API_KEY_FOR_CREATED_WEBHOOKS, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getBaseUrlForCreatedWebhooks(
        string $scope = ScopeInterface::SCOPE_STORE,
        $scopeCode = null
    ): ?string {
        return $this->scopeConfig->getValue(self::BASE_URL_FOR_CREATED_WEBHOOKS, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getWebhookAuthTokenForCreatedWebhooks(
        string $scope = ScopeInterface::SCOPE_STORE,
        $scopeCode = null
    ): ?string {
        return $this->scopeConfig->getValue(self::WEBHOOK_AUTH_TOKEN_FOR_CREATED_WEBHOOKS, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return string|null
     */
    public function getWebhookAuthTokenHeaderName(
        string $scope = ScopeInterface::SCOPE_STORE,
        $scopeCode = null
    ): ?string {
        return $this->scopeConfig->getValue(self::WEBHOOK_AUTH_TOKEN_HEADER_NAME, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param null $scopeCode
     * @return string
     */
    public function getBaseUrl(
        string $scope = ScopeInterface::SCOPE_STORE,
        $scopeCode = null
    ): string {
        $isSecure = $this->scopeConfig->isSetFlag(
            Store::XML_PATH_SECURE_IN_FRONTEND,
            $scope,
            $scopeCode
        );
        $configPath = $isSecure ? Store::XML_PATH_SECURE_BASE_URL : Store::XML_PATH_UNSECURE_BASE_URL;

        $url = $this->scopeConfig->getValue(
            $configPath,
            $scope,
            $scopeCode
        );
        if (strpos($url, Store::BASE_URL_PLACEHOLDER) !== false) {
            $url = str_replace(Store::BASE_URL_PLACEHOLDER, $this->request->getDistroBaseUrl(), $url);
        }

        return $url;
    }

    /**
     * @param string $methodName
     * @param string|null $id
     * @param bool $maskId
     * @return string
     * @throws LocalizedException
     */
    public function getUri(string $methodName, ?string $id = null, bool $maskId = false): string
    {
        $apiUrl = $this->getApiUrl();
        if (!$apiUrl) {
            throw new LocalizedException(__('API URL is not set.'));
        }

        $apiEndpoint = $apiUrl . ApiClient::API_URL_PATH . $methodName;
        if ($id) {
            if ($maskId && $methodName === ApiClient::API_PATH_BUYERS) {
                $id = $this->maskValue->mask((string)$id);
            }

            $apiEndpoint .= '/' . $id;
            if ($methodName === ApiClient::API_PATH_BUYERS) {
                $apiEndpoint .= '/status';
            }
        }

        return $apiEndpoint;
    }
}
