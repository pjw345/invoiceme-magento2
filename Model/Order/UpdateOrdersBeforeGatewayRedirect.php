<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Order;

use Exception;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Msts\InvoiceMe\Model\ConfigProvider;
use Psr\Log\LoggerInterface;

class UpdateOrdersBeforeGatewayRedirect
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var GetOrdersByEntityIds
     */
    private $getOrdersByEntityIds;

    /**
     * @var AddCommentToHistory
     */
    private $addCommentToHistory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        GetOrdersByEntityIds $getOrdersByEntityIds,
        AddCommentToHistory $addCommentToHistory,
        LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->getOrdersByEntityIds = $getOrdersByEntityIds;
        $this->addCommentToHistory = $addCommentToHistory;
        $this->logger = $logger;
    }

    public function execute(array $orderIds): void
    {
        $orders = $this->getOrdersByEntityIds->execute($orderIds);
        foreach ($orders as $order) {
            try {
                $order->setState(Order::STATE_PENDING_PAYMENT);
                $order->setStatus(ConfigProvider::ORDER_STATUS_PENDING_INVOICEME);
                $this->orderRepository->save($order);
                $this->addCommentToHistory->execute(
                    $order,
                    __('Customer redirected to the InvoiceMe credit application form.')
                );
            } catch (Exception $e) {
                $this->logger->critical($e->getMessage(), ['exception' => $e]);
            }
        }
    }
}
