<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Msts\CaaS\Api\Data\Buyer\BuyerResponseInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\CurrencyConverter;

class UpdateCustomer
{
    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    private $customerResourceModel;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    public function __construct(
        EncryptorInterface $encryptor,
        CustomerRepositoryInterface $customerRepository,
        CurrencyConverter $currencyConverter,
        \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel,
        CaaSFactory $caaSFactory
    ) {
        $this->encryptor = $encryptor;
        $this->customerRepository = $customerRepository;
        $this->currencyConverter = $currencyConverter;
        $this->customerResourceModel = $customerResourceModel;
        $this->caaSFactory = $caaSFactory;
    }

    /**
     * @param Customer $customer
     * @throws LocalizedException
     * @throws ApiClientException
     */
    public function execute(Customer $customer): void
    {
        $mstsImBuyerId = $this->encryptor->decrypt($customer->getData('msts_im_buyer_id'));
        if (!$mstsImBuyerId) {
            return;
        }

        $caaS = $this->caaSFactory->create();
        $buyerResponse = $caaS->buyer->retrieve($mstsImBuyerId);
        $customer->setMstsImBusinessName($buyerResponse->getBusinessName());

        $statusOptionId = null;
        $mstsImStatus = $this->customerResourceModel->getAttribute('msts_im_status');
        if ($mstsImStatus->usesSource()) {
            $statusOptionId = $mstsImStatus->getSource()->getOptionId($buyerResponse->getStatus());
        }
        $customer->setMstsImStatus($statusOptionId);
        $currency = $this->getCurrency($buyerResponse);
        $multiplier = $this->currencyConverter->getMultiplier($currency);
        $customer->setMstsImCurrency($currency);
        $customer->setMstsImCreditApproved($buyerResponse->getCreditApproved() / $multiplier);
        $customer->setMstsImCreditBalance($buyerResponse->getCreditBalance() / $multiplier);
        $customer->setMstsImCreditPreauthorized($buyerResponse->getCreditPreauthorized() / $multiplier);

        $this->customerRepository->save($customer->getDataModel());
    }

    private function getCurrency(BuyerResponseInterface $buyerResponse): ?string
    {
        $currency = $buyerResponse->getCurrency();
        if (is_array($currency)) {
            $currency = reset($currency);
        }

        return $currency;
    }
}
