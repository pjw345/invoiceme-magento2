<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Customer;

use Magento\Customer\Api\Data\CustomerInterface;

class IsRegisteredMstsBuyer
{
    public function execute(CustomerInterface $customer): bool
    {
        return $customer->getCustomAttribute('msts_im_status') !== null;
    }
}
