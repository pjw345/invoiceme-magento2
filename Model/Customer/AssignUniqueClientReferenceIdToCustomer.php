<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\InputMismatchException;
use Msts\InvoiceMe\Exception\NotUniqueClientReferenceIdException;
use Msts\InvoiceMe\Model\UuidGenerator;

class AssignUniqueClientReferenceIdToCustomer
{
    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    public function __construct(
        UuidGenerator $uuidGenerator,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->uuidGenerator = $uuidGenerator;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Customer $customer
     * @param int $attemptNumber
     * @return string
     * @throws NotUniqueClientReferenceIdException
     * @throws InputException
     * @throws LocalizedException
     * @throws InputMismatchException
     */
    public function execute(Customer $customer, int $attemptNumber = 0): string
    {
        try {
            $clientReferenceId = $this->uuidGenerator->execute();
            $customer->setMstsImClientReferenceId($clientReferenceId);
            $this->customerRepository->save($customer->getDataModel());
        } catch (LocalizedException | NotUniqueClientReferenceIdException $e) {
            if ($attemptNumber >= 10) {
                throw new NotUniqueClientReferenceIdException(
                    __('Could not assign a unique Client Reference ID to the Customer.')
                );
            }
            $attemptNumber++;

            return $this->execute($customer, $attemptNumber);
        }

        return $clientReferenceId;
    }
}
