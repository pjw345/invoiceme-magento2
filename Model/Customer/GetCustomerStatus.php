<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Customer;

use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;

class GetCustomerStatus
{
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    private $customerResourceModel;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel
    ) {
        $this->customerResourceModel = $customerResourceModel;
    }

    /**
     * @param Customer $customer
     * @return string|null
     * @throws LocalizedException
     */
    public function execute(Customer $customer): ?string
    {
        $mstsImStatus = $this->customerResourceModel->getAttribute('msts_im_status');
        if ($mstsImStatus->usesSource()) {
            $optionText = $mstsImStatus->getSource()->getOptionText($customer->getData('msts_im_status'));
            if ($optionText === false) {
                return null;
            }

            return $optionText;
        }

        return null;
    }
}
