<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class GetCustomerByClientReferenceId
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param string $clientReferenceId
     * @return CustomerInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function execute(string $clientReferenceId): CustomerInterface
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'msts_im_client_reference_id',
            $clientReferenceId
        )->create();
        $customers = $this->customerRepository->getList($searchCriteria)->getItems();
        $customer = reset($customers);
        if (!$customer) {
            throw new NoSuchEntityException(
                __("The entity that was requested doesn't exist. Verify the entity and try again.")
            );
        }

        return $customer;
    }
}
