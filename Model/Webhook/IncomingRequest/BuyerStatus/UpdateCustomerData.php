<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus;

use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResourceModel;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\InputMismatchException;
use Msts\InvoiceMe\Exception\Webhook\BuyerStatus\InvalidStatusException;
use Msts\InvoiceMe\Model\CurrencyConverter;

class UpdateCustomerData
{
    /**
     * @var CustomerResourceModel
     */
    private $customerResourceModel;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;

    public function __construct(
        CustomerResourceModel $customerResourceModel,
        CustomerRepositoryInterface $customerRepository,
        EncryptorInterface $encryptor,
        CurrencyConverter $currencyConverter
    ) {
        $this->customerResourceModel = $customerResourceModel;
        $this->customerRepository = $customerRepository;
        $this->encryptor = $encryptor;
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * @param CustomerInterface $customer
     * @param array $data
     * @throws InputException
     * @throws LocalizedException
     * @throws InputMismatchException
     * @throws InvalidStatusException
     */
    public function execute(CustomerInterface $customer, array $data): void
    {
        $customer->setCustomAttribute('msts_im_buyer_id', $this->encryptor->encrypt($data['id']));
        $customer->setCustomAttribute('msts_im_business_name', $data['business_name']);

        $statusOptionId = $this->customerResourceModel->getAttribute('msts_im_status')
            ->getSource()
            ->getOptionId($data['status']);
        if (!$statusOptionId) {
            throw new InvalidStatusException(
                sprintf(
                    "The sent value '%s' for the 'status' field is invalid. List of valid values: %s",
                    $data['status'],
                    $this->formatValidStatuses($this->getValidBuyerStatuses())
                )
            );
        }
        $customer->setCustomAttribute('msts_im_status', $statusOptionId);

        $customer->setCustomAttribute('msts_im_currency', $data['currency']);
        $multiplier = $this->currencyConverter->getMultiplier($data['currency']);

        $customer->setCustomAttribute('msts_im_credit_approved', $data['credit_approved'] / $multiplier);
        $customer->setCustomAttribute('msts_im_credit_balance', $data['credit_balance'] / $multiplier);
        $customer->setCustomAttribute('msts_im_credit_preauthorized', $data['credit_preauthorized'] / $multiplier);

        $this->customerRepository->save($customer);
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    private function getValidBuyerStatuses(): array
    {
        $statusOptions = $this->customerResourceModel->getAttribute('msts_im_status')->getSource()->toOptionArray();
        $validStatuses = [];
        foreach ($statusOptions as $statusOption) {
            if (!$statusOption['value']) {
                continue;
            }
            $validStatuses[] = $statusOption['label'];
        }

        return $validStatuses;
    }

    private function formatValidStatuses(array $validStatuses): string
    {
        if (empty($validStatuses)) {
            return '';
        }

        return "'" . implode("', '", $validStatuses) . "'";
    }
}
