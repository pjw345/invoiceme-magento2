<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus;

use Magento\Customer\Model\ResourceModel\Customer as CustomerResourceModel;
use Magento\Framework\Exception\LocalizedException;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;

class ValidateIfCustomerBecameActiveBuyer
{
    /**
     * @var CustomerResourceModel
     */
    private $customerResourceModel;

    public function __construct(
        CustomerResourceModel $customerResourceModel
    ) {
        $this->customerResourceModel = $customerResourceModel;
    }

    /**
     * @param string $newStatus
     * @param string|null $oldStatusOptionId
     * @return bool
     * @throws LocalizedException
     */
    public function execute(string $newStatus, ?string $oldStatusOptionId): bool
    {
        if ($newStatus !== BuyerStatusInterface::ACTIVE) {
            return false;
        }

        $newStatusOptionId = $this->customerResourceModel
            ->getAttribute('msts_im_status')
            ->getSource()
            ->getOptionId($newStatus);

        return $oldStatusOptionId === null || $oldStatusOptionId !== $newStatusOptionId;
    }
}
