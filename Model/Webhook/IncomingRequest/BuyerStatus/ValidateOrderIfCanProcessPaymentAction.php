<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Msts\InvoiceMe\Model\ConfigProvider;

class ValidateOrderIfCanProcessPaymentAction
{
    /**
     * @param OrderInterface $order
     * @param CustomerInterface $customer
     * @throws LocalizedException
     */
    public function execute(OrderInterface $order, CustomerInterface $customer): void
    {
        if ($customer->getId() != $order->getCustomerId()) {
            throw new LocalizedException(
                __('Order is not associated with the provided customer.')
            );
        }

        $buyerIdAttribute = $customer->getCustomAttribute('msts_im_buyer_id');
        if (!$buyerIdAttribute || !$buyerIdAttribute->getValue()) {
            throw new LocalizedException(
                __('Customer does not have the buyer ID assigned.')
            );
        }

        $payment = $order->getPayment();
        if (!$payment) {
            throw new LocalizedException(
                __('Order has no payment method.')
            );
        }

        $method = $payment->getMethodInstance();
        if ($method->getCode() !== ConfigProvider::CODE) {
            throw new LocalizedException(
                __('Order payment method is not InvoiceMe.')
            );
        }
    }
}
