<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus;

use Exception;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Model\MethodInterface;
use Magento\Store\Model\ScopeInterface;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Order\AddCommentToHistory;
use Msts\InvoiceMe\Model\Order\GetCustomerPendingOrders;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProcessPaymentActionsForPastOrders
{
    /**
     * @var GetCustomerPendingOrders
     */
    private $getCustomerPendingOrders;
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var ValidateOrderIfCanProcessPaymentAction
     */
    private $validateOrderIfCanProcessPaymentAction;

    /**
     * @var ProcessPaymentActionForOrder
     */
    private $processPaymentActionForOrder;

    /**
     * @var AddCommentToHistory
     */
    private $addCommentToHistory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        GetCustomerPendingOrders $getCustomerPendingOrders,
        ConfigProvider $configProvider,
        ValidateOrderIfCanProcessPaymentAction $validateOrderIfCanProcessPaymentAction,
        ProcessPaymentActionForOrder $processPaymentActionForOrder,
        AddCommentToHistory $addCommentToHistory,
        LoggerInterface $logger
    ) {
        $this->getCustomerPendingOrders = $getCustomerPendingOrders;
        $this->configProvider = $configProvider;
        $this->validateOrderIfCanProcessPaymentAction = $validateOrderIfCanProcessPaymentAction;
        $this->processPaymentActionForOrder = $processPaymentActionForOrder;
        $this->addCommentToHistory = $addCommentToHistory;
        $this->logger = $logger;
    }

    public function execute(CustomerInterface $customer): void
    {
        $orders = $this->getCustomerPendingOrders->execute($customer);
        foreach ($orders as $order) {
            $paymentAction = $this->configProvider->getPaymentAction(
                ScopeInterface::SCOPE_STORE,
                $order->getStoreId()
            );
            $paymentActionName = $this->getPaymentActionName($paymentAction);
            try {
                try {
                    $this->validateOrderIfCanProcessPaymentAction->execute($order, $customer);
                    $this->processPaymentActionForOrder->execute($order, $paymentAction);
                } catch (LocalizedException $e) {
                    $this->addCommentToHistory->execute(
                        $order,
                        __('InvoiceMe payment %1 error: %2', $paymentActionName, $e->getMessage())
                    );
                } catch (Exception $e) {
                    $this->logger->critical($e->getMessage(), ['exception' => $e]);
                    $this->addCommentToHistory->execute(
                        $order,
                        __(
                            'Core error occurred during InvoiceMe payment %1. Please check the exception log for details.',
                            $paymentActionName
                        )
                    );
                }
            } catch (CouldNotSaveException $e) {
                $this->logger->critical($e->getMessage(), ['exception' => $e]);
            }
        }
    }

    private function getPaymentActionName(string $paymentAction): string
    {
        switch ($paymentAction) {
            case MethodInterface::ACTION_AUTHORIZE:
                return (string)__('preauthorization');
            case MethodInterface::ACTION_AUTHORIZE_CAPTURE:
                return (string)__('charge');
            default:
                return '';
        }
    }
}
