<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Payment\Transaction;
use Msts\InvoiceMe\Api\Data\Preauthorization\PreauthorizationStatusInterface;

class ProcessPreauthorization
{
    /**
     * @var UpdateOrderAndNotifyCustomer
     */
    private $updateOrderAndNotifyCustomer;

    public function __construct(UpdateOrderAndNotifyCustomer $updateOrderAndNotifyCustomer)
    {
        $this->updateOrderAndNotifyCustomer = $updateOrderAndNotifyCustomer;
    }

    /**
     * @param OrderInterface $order
     * @param string $paymentAction
     * @throws LocalizedException
     * @throws Exception
     */
    public function execute(OrderInterface $order, string $paymentAction): void
    {
        $payment = $order->getPayment();
        $payment->authorize(true, $payment->getBaseAmountOrdered());

        $transactionAdditionalInfo = $payment->getTransactionAdditionalInfo();
        if (!isset($transactionAdditionalInfo[Transaction::RAW_DETAILS])
            || !isset($transactionAdditionalInfo[Transaction::RAW_DETAILS]['status'])) {
            throw new LocalizedException(__('Status of the transaction not found.'));
        }

        $preauthorizationStatus = $transactionAdditionalInfo[Transaction::RAW_DETAILS]['status'];
        if ($preauthorizationStatus === PreauthorizationStatusInterface::PREAUTHORIZED) {
            $this->updateOrderAndNotifyCustomer->execute($order, $paymentAction);
        }
    }
}
