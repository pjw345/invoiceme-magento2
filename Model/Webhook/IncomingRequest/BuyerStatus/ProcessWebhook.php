<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;
use Msts\InvoiceMe\Api\Data\Webhook\EventTypeInterface;
use Msts\InvoiceMe\Exception\Webhook\BuyerStatus\InvalidStatusException;
use Msts\InvoiceMe\Exception\Webhook\SchemaValidationException;
use Msts\InvoiceMe\Model\Customer\GetCustomerByClientReferenceId;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\ValidateInputData;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProcessWebhook
{
    /**
     * @var ValidateInputData
     */
    private $validateInputData;

    /**
     * @var GetCustomerByClientReferenceId
     */
    private $getCustomerByClientReferenceId;

    /**
     * @var UpdateCustomerData
     */
    private $updateCustomerData;

    /**
     * @var ValidateIfCustomerBecameActiveBuyer
     */
    private $validateIfCustomerBecameActiveBuyer;

    /**
     * @var ProcessPaymentActionsForPastOrders
     */
    private $processPaymentActionsForPastOrders;

    public function __construct(
        ValidateInputData $validateInputData,
        GetCustomerByClientReferenceId $getCustomerByClientReferenceId,
        UpdateCustomerData $updateCustomerData,
        ValidateIfCustomerBecameActiveBuyer $validateIfCustomerBecameActiveBuyer,
        ProcessPaymentActionsForPastOrders $processPaymentActionsForPastOrders
    ) {
        $this->validateInputData = $validateInputData;
        $this->getCustomerByClientReferenceId = $getCustomerByClientReferenceId;
        $this->updateCustomerData = $updateCustomerData;
        $this->validateIfCustomerBecameActiveBuyer = $validateIfCustomerBecameActiveBuyer;
        $this->processPaymentActionsForPastOrders = $processPaymentActionsForPastOrders;
    }

    /**
     * @param array $inputData
     * @throws SchemaValidationException
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InputMismatchException
     * @throws InvalidStatusException
     */
    public function execute(array $inputData): void
    {
        $this->validateInputData->execute($inputData, EventTypeInterface::BUYER_STATUS);
        $customer = $this->getCustomerByClientReferenceId->execute($inputData['data']['client_reference_id']);
        $newStatus = $inputData['data']['status'];
        $buyerStatusAttribute = $customer->getCustomAttribute('msts_im_status');
        $oldStatusOptionId = $buyerStatusAttribute ? (string)$buyerStatusAttribute->getValue() : '';
        $this->updateCustomerData->execute($customer, $inputData['data']);

        if ($this->validateIfCustomerBecameActiveBuyer->execute($newStatus, $oldStatusOptionId)) {
            $this->processPaymentActionsForPastOrders->execute($customer);
        }
    }
}
