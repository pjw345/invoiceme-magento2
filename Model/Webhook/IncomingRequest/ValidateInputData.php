<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest;

use Msts\InvoiceMe\Api\Data\Webhook\EventTypeInterface;
use Msts\InvoiceMe\Exception\Webhook\SchemaValidationException;

class ValidateInputData
{
    /**
     * @var array
     */
    private $requiredDataKeys = [
        EventTypeInterface::BUYER_STATUS => [
            'id',
            'business_name',
            'client_reference_id',
            'status',
            'currency',
            'credit_approved',
            'credit_balance',
            'credit_preauthorized',
        ],
        EventTypeInterface::PRE_AUTHORIZATION_UPDATED => [
            'id',
            'status',
            'currency',
            'preauthorized_amount',
            'captured_amount',
            'expires',
        ],
    ];

    /**
     * @param array $inputData
     * @param string $eventType
     * @return bool
     * @throws SchemaValidationException
     */
    public function execute(array $inputData, string $eventType): bool
    {
        if (!is_array($inputData)
            || !isset($inputData['event_type'])
            || !isset($inputData['data'])
            || !isset($inputData['timestamp'])
            || !($inputData['event_type'] === $eventType)
            || !$this->validateRequiredDataKeys($inputData['data'], $eventType)) {
            throw new SchemaValidationException('Request body failed JSON schema validation');
        }

        return true;
    }

    /**
     * @param string[] $inputData
     * @param string $eventType
     * @return bool
     */
    private function validateRequiredDataKeys(array $inputData, string $eventType): bool
    {
        $requiredDataKeys = $this->requiredDataKeys[$eventType] ?? [];
        foreach ($requiredDataKeys as $key) {
            if (!isset($inputData[$key])) {
                return false;
            }
        }

        return true;
    }
}
