<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\PreauthorizationUpdated;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Msts\InvoiceMe\Api\Data\Webhook\EventTypeInterface;
use Msts\InvoiceMe\Exception\Webhook\SchemaValidationException;
use Msts\InvoiceMe\Model\Order\Payment\GetTransactionByTransactionId;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\ValidateInputData;

class ProcessWebhook
{
    /**
     * @var ValidateInputData
     */
    private $validateInputData;

    /**
     * @var GetTransactionByTransactionId
     */
    private $getTransactionByTransactionId;

    /**
     * @var UpdatePaymentBaseAmountAuthorized
     */
    private $updatePaymentBaseAmountAuthorized;

    /**
     * @var UpdateTransactionAdditionalInformation
     */
    private $updateTransactionAdditionalInformation;

    public function __construct(
        GetTransactionByTransactionId $getTransactionByTransactionId,
        ValidateInputData $validateInputData,
        UpdatePaymentBaseAmountAuthorized $updatePaymentBaseAmountAuthorized,
        UpdateTransactionAdditionalInformation $updateTransactionAdditionalInformation
    ) {
        $this->validateInputData = $validateInputData;
        $this->getTransactionByTransactionId = $getTransactionByTransactionId;
        $this->updatePaymentBaseAmountAuthorized = $updatePaymentBaseAmountAuthorized;
        $this->updateTransactionAdditionalInformation = $updateTransactionAdditionalInformation;
    }

    /**
     * @param array $inputData
     * @throws NoSuchEntityException
     * @throws LocalizedException
     * @throws SchemaValidationException
     */
    public function execute(array $inputData): void
    {
        $this->validateInputData->execute($inputData, EventTypeInterface::PRE_AUTHORIZATION_UPDATED);
        $transaction = $this->getTransactionByTransactionId->execute($inputData['data']['id']);
        if (!$transaction->getIsClosed()) {
            $this->updatePaymentBaseAmountAuthorized->execute(
                (int)$transaction->getPaymentId(),
                (int)$inputData['data']['preauthorized_amount'],
                $inputData['data']['currency']
            );
        }

        $this->updateTransactionAdditionalInformation->execute($transaction, $inputData);
    }
}
