<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook\IncomingRequest\PreauthorizationUpdated;

use Magento\Sales\Api\OrderPaymentRepositoryInterface;
use Msts\InvoiceMe\Model\GetAmountFromSubunits;

class UpdatePaymentBaseAmountAuthorized
{
    /**
     * @var OrderPaymentRepositoryInterface
     */
    private $orderPaymentRepository;

    /**
     * @var GetAmountFromSubunits
     */
    private $getAmountFromSubunits;

    public function __construct(
        OrderPaymentRepositoryInterface $orderPaymentRepository,
        GetAmountFromSubunits $getAmountFromSubunits
    ) {
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->getAmountFromSubunits = $getAmountFromSubunits;
    }

    public function execute(int $paymentId, int $preauthorizedAmount, string $currency): void
    {
        $payment = $this->orderPaymentRepository->get($paymentId);
        $baseAmountAuthorized = $this->getAmountFromSubunits->execute($preauthorizedAmount, $currency);
        $payment->setBaseAmountAuthorized($baseAmountAuthorized);
        $this->orderPaymentRepository->save($payment);
    }
}
