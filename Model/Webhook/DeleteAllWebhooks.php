<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook;

use Magento\Store\Model\ScopeInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\ConfigProvider;

class DeleteAllWebhooks
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    public function __construct(
        ConfigProvider $configProvider,
        CaaSFactory $caaSFactory
    ) {
        $this->configProvider = $configProvider;
        $this->caaSFactory = $caaSFactory;
    }

    /**
     * @param string $scope
     * @param mixed $scopeId
     * @throws ApiClientException
     */
    public function execute(string $scope = ScopeInterface::SCOPE_STORE, $scopeId = null): void
    {
        $webhooks = $this->configProvider->getCreatedWebhooks($scope, $scopeId);
        if (!$webhooks) {
            return;
        }

        $apiKey = $this->configProvider->getApiKeyForCreatedWebhooks($scope, $scopeId);
        $caaS = $this->caaSFactory->create([], $scope, $scopeId);
        foreach ($webhooks as $index => $webhook) {
            if (is_array($webhook)) {
                if (isset($webhook['id'])) {
                    $caaS->webhooks->delete($webhook['id'], $apiKey);
                }
            } elseif ($index === 'id') {
                $caaS->webhooks->delete($webhook, $apiKey);
                break;
            }
        }
    }
}
