<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Webhook;

use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;
use Msts\CaaS\Api\Data\Webhook\WebhookInterface;
use Msts\CaaS\Api\Data\Webhook\WebhookInterfaceFactory;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Api\Data\Webhook\EventTypeInterface;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\UuidGenerator;

class CreateWebhooks
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    /**
     * @var WebhookInterfaceFactory
     */
    private $webhookFactory;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    public function __construct(
        ConfigProvider $configProvider,
        UuidGenerator $uuidGenerator,
        WebhookInterfaceFactory $webhookFactory,
        CaaSFactory $caaSFactory
    ) {
        $this->configProvider = $configProvider;
        $this->uuidGenerator = $uuidGenerator;
        $this->webhookFactory = $webhookFactory;
        $this->caaSFactory = $caaSFactory;
    }

    /**
     * @param string $scope
     * @param int|null $scopeId
     * @return WebhookInterface[]
     * @throws ApiClientException
     * @throws LocalizedException
     */
    public function execute(string $scope = ScopeInterface::SCOPE_STORE, ?int $scopeId = null): array
    {
        $authToken = 'Bearer ' . $this->uuidGenerator->execute();
        $webhooks = [];
        $webhooks[] = $this->createWebhook(
            $authToken,
            EventTypeInterface::BUYER_STATUS,
            'buyerStatus',
            $scope,
            $scopeId
        );
        $webhooks[] = $this->createWebhook(
            $authToken,
            EventTypeInterface::PRE_AUTHORIZATION_UPDATED,
            'preauthorizationUpdated',
            $scope,
            $scopeId
        );

        return $webhooks;
    }

    /**
     * @param string $scope
     * @param int|null $scopeId
     * @param string $authToken
     * @param string $eventType
     * @param string $actionName
     * @return WebhookInterface
     * @throws ApiClientException
     */
    private function createWebhook(
        string $authToken,
        string $eventType,
        string $actionName,
        string $scope,
        ?int $scopeId
    ): WebhookInterface {
        $baseUrl = $this->configProvider->getBaseUrl($scope, $scopeId);
        /** @var WebhookInterface $webhook */
        $webhook = $this->webhookFactory->create();
        $webhook->setUrl($this->getWebhookUrl($baseUrl, $actionName));
        $webhook->setEventTypes([$eventType]);
        $webhook->setAuthToken($authToken);
        $webhook->setAuthTokenHeader((string)$this->configProvider->getWebhookAuthTokenHeaderName($scope, $scopeId));

        $caaS = $this->caaSFactory->create([], $scope, $scopeId);

        return $caaS->webhooks->create($webhook->getRequestData());
    }

    private function getWebhookUrl(string $baseUrl, string $eventType): string
    {
        return $baseUrl . 'msts_invoiceme/webhook/' . $eventType;
    }
}
