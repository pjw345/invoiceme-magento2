<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model;

use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Msts\CaaS\CaaS;
use Msts\CaaS\CaaSOptions;
use Msts\CaaS\Model\ClientConfigProvider;
use Msts\CaaS\Model\Http\MstsRequestFactory;
use Msts\CaaS\Model\MaskValue;
use Psr\Log\LoggerInterface;

class CaaSFactory
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var string
     */
    private $instanceName;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MaskValue
     */
    private $maskValue;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var MstsRequestFactory
     */
    private $mstsRequestFactory;

    /**
     * @var ClientConfigProvider
     */
    private $clientConfigProvider;

    public function __construct(
        ObjectManagerInterface $objectManager,
        LoggerInterface $logger,
        MaskValue $maskValue,
        MstsRequestFactory $mstsRequestFactory,
        ConfigProvider $configProvider,
        ClientConfigProvider $clientConfigProvider,
        string $instanceName = CaaS::class
    ) {
        $this->objectManager = $objectManager;
        $this->instanceName = $instanceName;
        $this->logger = $logger;
        $this->maskValue = $maskValue;
        $this->configProvider = $configProvider;
        $this->mstsRequestFactory = $mstsRequestFactory;
        $this->clientConfigProvider = $clientConfigProvider;
    }

    /**
     * @param array $data
     * @param string $scope
     * @param mixed|null $scopeCode
     * @return CaaS
     */
    public function create(
        array $data = [],
        string $scope = ScopeInterface::SCOPE_STORE,
        $scopeCode = null
    ): CaaS {
        $data['apiKey'] = $this->configProvider->getApiKey($scope, $scopeCode);
        /** @var CaaSOptions $caaSOptions */
        $caaSOptions = $this->objectManager->create(CaaSOptions::class, $data);
        $caaSOptions->setLogger($this->logger);
        $caaSOptions->setMaskValue($this->maskValue);
        $clientConfigProvider = $this->clientConfigProvider;
        $clientConfigProvider->setBaseUri($this->configProvider->getApiUrl($scope, $scopeCode));
        $mstsRequest = $this->mstsRequestFactory->create(['configProvider' => $clientConfigProvider]);
        $caaSOptions->setRequestClass($mstsRequest);
        $data['options'] = $caaSOptions;

        return $this->objectManager->create($this->instanceName, $data);
    }
}
