<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResourceModel;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;
use Msts\InvoiceMe\Gateway\Request\TransactionDetails;
use Msts\InvoiceMe\Model\ConfigProvider as Config;

class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var CustomerResourceModel
     */
    private $customerResourceModel;

    public function __construct(
        UrlInterface $urlBuilder,
        CustomerResourceModel $customerResourceModel
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->customerResourceModel = $customerResourceModel;
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    public function getConfig(): array
    {
        return [
            'payment' => [
                Config::CODE => [
                    'buyerStatusActiveOptionId' => $this->getOptionIdOfBuyerStatusActive(),
                    'redirectUrl' => $this->urlBuilder->getUrl('msts_invoiceme/gateway/redirect'),
                    'invoiceMeSectionUrl' => $this->urlBuilder->getUrl('msts_invoiceme/customer/'),
                    'invoiceme_po_number' => [
                        'maxlength' => TransactionDetails::FORM_FIELD_PO_NUMBER_MAXIMUM_LENGTH,
                    ],
                    'invoiceme_notes' => [
                        'maxlength' => TransactionDetails::FORM_FIELD_NOTES_MAXIMUM_LENGTH,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    private function getOptionIdOfBuyerStatusActive(): string
    {
        $mstsImStatusAttribute = $this->customerResourceModel->getAttribute('msts_im_status');
        if ($mstsImStatusAttribute && $mstsImStatusAttribute->usesSource()) {
            return $mstsImStatusAttribute->getSource()
                ->getOptionId(BuyerStatusInterface::ACTIVE);
        }

        return '';
    }
}
