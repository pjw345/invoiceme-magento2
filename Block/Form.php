<?php

declare(strict_types=1);

namespace Msts\InvoiceMe\Block;

use Magento\Backend\Model\Session\Quote;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResourceModel;
use Magento\Framework\View\Element\Template\Context;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;

class Form extends \Magento\Payment\Block\Form
{
    /**
     * @var Quote
     */
    private $quoteSession;

    /**
     * @var CustomerResourceModel
     */
    private $customerResourceModel;

    public function __construct(
        Quote $quoteSession,
        CustomerResourceModel $customerResourceModel,
        Context $context,
        array $data = []
    ) {
        $this->quoteSession = $quoteSession;
        $this->customerResourceModel = $customerResourceModel;
        parent::__construct($context, $data);
    }

    public function getValidationMessage(): string
    {
        $buyerStatus = $this->getBuyerStatus();
        if (!$buyerStatus) {
            return (string) __('InvoiceMe payment method is currently not available to this customer as the customer is not registered in MSTS yet.');
        }

        if ($buyerStatus && !$this->isCustomerActiveBuyer()) {
            return (string) __('InvoiceMe payment method is currently not available to this customer as their MSTS status is "%1".', $buyerStatus);
        }

        return '';
    }

    public function isCustomerActiveBuyer(): bool
    {
        return $this->getBuyerStatus() === BuyerStatusInterface::ACTIVE;
    }

    private function getBuyerStatus(): ?string
    {
        $mstsImStatus = $this->customerResourceModel->getAttribute('msts_im_status');
        if ($mstsImStatus->usesSource()) {
            $statusAttribute = $this->getCustomer()->getCustomAttribute('msts_im_status');
            if (!$statusAttribute) {
                return null;
            }

            $optionText = $mstsImStatus->getSource()->getOptionText($statusAttribute->getValue());
            if ($optionText === false) {
                return null;
            }

            return $optionText;
        }

        return null;
    }

    private function getCustomer(): CustomerInterface
    {
        return $this->quoteSession->getQuote()->getCustomer();
    }
}
