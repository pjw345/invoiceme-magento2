<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Ui\Component\Fieldset;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Form\Fieldset;
use Msts\InvoiceMe\Model\ConfigProvider;

class InvoiceMe extends Fieldset implements ComponentVisibilityInterface
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(
        ConfigProvider $configProvider,
        RequestInterface $request,
        ContextInterface $context,
        array $components = [],
        array $data = []
    ) {
        $this->configProvider = $configProvider;
        $this->request = $request;
        parent::__construct($context, $components, $data);
    }

    public function isComponentVisible(): bool
    {
        return $this->configProvider->isActive() && $this->request->getParam('id');
    }
}
