<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\ViewModel\Customer;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\Information;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Customer\GetCustomerStatus;
use Msts\InvoiceMe\Model\Customer\UpdateCustomer;
use Msts\InvoiceMe\Model\PriceFormatter;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomerInvoiceMe implements ArgumentInterface
{
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var UpdateCustomer
     */
    private $updateCustomer;

    /**
     * @var bool
     */
    private $customerHasBeenUpdated = false;

    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @var GetCustomerStatus
     */
    private $getCustomerStatus;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Session $customerSession,
        ConfigProvider $configProvider,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        UpdateCustomer $updateCustomer,
        PriceFormatter $priceFormatter,
        GetCustomerStatus $getCustomerStatus,
        LoggerInterface $logger
    ) {
        $this->customerSession = $customerSession;
        $this->configProvider = $configProvider;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->updateCustomer = $updateCustomer;
        $this->priceFormatter = $priceFormatter;
        $this->getCustomerStatus = $getCustomerStatus;
        $this->logger = $logger;
    }

    public function isBuyerActive(): bool
    {
        return $this->getBuyerStatus() === BuyerStatusInterface::ACTIVE;
    }

    public function getBuyerStatus(): ?string
    {
        return $this->getCustomerStatus->execute($this->getCustomer());
    }

    public function getMstsImCreditApproved(): ?string
    {
        return $this->priceFormatter->getPriceFormatted(
            (float)$this->getCustomer()->getData('msts_im_credit_approved'),
            $this->getCustomer()->getMstsImCurrency()
        );
    }

    public function getMstsImCreditBalance(): ?string
    {
        return $this->priceFormatter->getPriceFormatted(
            (float)$this->getCustomer()->getData('msts_im_credit_balance'),
            $this->getCustomer()->getMstsImCurrency()
        );
    }

    public function getMstsImCreditPreauthorized(): ?string
    {
        return $this->priceFormatter->getPriceFormatted(
            (float)$this->getCustomer()->getData('msts_im_credit_preauthorized'),
            $this->getCustomer()->getMstsImCurrency()
        );
    }

    public function getMstsImCreditAvailable(): ?string
    {
        return $this->priceFormatter->getPriceFormatted(
            (
                (float)$this->getCustomer()->getData('msts_im_credit_approved')
                - (float)$this->getCustomer()->getData('msts_im_credit_balance')
                - (float)$this->getCustomer()->getData('msts_im_credit_preauthorized')
            ),
            $this->getCustomer()->getMstsImCurrency()
        );
    }

    public function shouldDisplayMessageOnly(): bool
    {
        return in_array(
            $this->getBuyerStatus(),
            [
                BuyerStatusInterface::CANCELLED,
                BuyerStatusInterface::DECLINED,
                BuyerStatusInterface::INACTIVE,
                BuyerStatusInterface::PENDING,
                BuyerStatusInterface::PENDING_DIRECT_DEBIT,
                BuyerStatusInterface::PENDING_SETUP,
                BuyerStatusInterface::SUSPENDED,
                BuyerStatusInterface::WITHDRAWN,
            ]
        );
    }

    /**
     * @return Phrase|null
     * @throws NoSuchEntityException
     */
    public function getMessage(): ?Phrase
    {
        $programUrl = $this->getBuyerPortalUrl();
        $creditApplicationUrl = $this->getApplyForCreditUrl();
        $message = null;
        switch ($this->getBuyerStatus()) {
            case BuyerStatusInterface::CANCELLED:
                $message = __(
                    'Sorry, your application has been cancelled. Please visit <a href="%1">%2</a> to resubmit.',
                    $creditApplicationUrl,
                    $creditApplicationUrl
                );
                break;
            case BuyerStatusInterface::DECLINED:
                $message = __(
                    'Sorry, your credit application has been denied. Please use another payment method for business purchases from %1.',
                    $this->getStoreName()
                );
                break;
            case BuyerStatusInterface::INACTIVE:
                $message = __(
                    'Oh no! Your InvoiceMe account is inactive. This can be for many reasons, please visit <a href="%1">%2</a> to resolve this matter.',
                    $programUrl,
                    $programUrl
                );
                break;
            case BuyerStatusInterface::PENDING:
                $message = __(
                    'We are currently reviewing your enrollment application for InvoiceMe. This process normally takes around 4 business hours to complete. If we have any questions or an approval, we will reach out to you via email.'
                );
                break;
            case BuyerStatusInterface::PENDING_DIRECT_DEBIT:
            case BuyerStatusInterface::PENDING_SETUP:
                $message = __(
                    'You have been approved to make purchases on terms! To activate your InvoiceMe account, please visit <a href="%1">%2</a> to complete your setup.',
                    $programUrl,
                    $programUrl
                );
                break;
            case BuyerStatusInterface::SUSPENDED:
                $message = __(
                    'Whoops! Your InvoiceMe account has been suspended. This is likely due to past due payments or needing a credit line increase. Please visit <a href="%1">%2</a> to resolve this matter.',
                    $programUrl,
                    $programUrl
                );
                break;
            case BuyerStatusInterface::WITHDRAWN:
                $message = __(
                    'You chose to withdraw your application. Change of mind? Visit <a href="%1">%2</a> to re-apply at any time.',
                    $creditApplicationUrl,
                    $creditApplicationUrl
                );
                break;
        }

        return $message;
    }

    public function getBuyerPortalUrl(): ?string
    {
        return $this->configProvider->getProgramUrl();
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    private function getStoreName(): string
    {
        return $this->scopeConfig->getValue(
            Information::XML_PATH_STORE_INFO_NAME,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

    public function getApplyForCreditUrl(): ?string
    {
        return $this->urlBuilder->getUrl('msts_invoiceme/customer/applyForCredit');
    }

    private function getCustomer(): Customer
    {
        $customer = $this->customerSession->getCustomer();
        if (!$this->customerHasBeenUpdated
            && $this->getCustomerStatus->execute($customer) === BuyerStatusInterface::ACTIVE) {
            try {
                $this->updateCustomer->execute($customer);
            } catch (LocalizedException | ApiClientException $e) {
                $this->logger->critical($e);
            }
            $this->customerHasBeenUpdated = true;
        }

        return $customer;
    }
}
