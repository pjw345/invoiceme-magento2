<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Sales\Model\Order;
use Msts\InvoiceMe\Model\ConfigProvider;

class AddOrderStatus implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    public function __construct(ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getTable('sales_order_status'),
            ['status', 'label'],
            [
                ['status' => ConfigProvider::ORDER_STATUS_PENDING_INVOICEME, 'label' => __('Pending InvoiceMe')],
            ]
        );

        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default', 'visible_on_front'],
            [
                [
                    'status' => ConfigProvider::ORDER_STATUS_PENDING_INVOICEME,
                    'state' => Order::STATE_PENDING_PAYMENT,
                    'is_default' => 0,
                    'visible_on_front' => 1,
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function revert()
    {
        $this->moduleDataSetup->getConnection()->delete(
            $this->moduleDataSetup->getTable('sales_order_status'),
            ['status' => ConfigProvider::ORDER_STATUS_PENDING_INVOICEME]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
