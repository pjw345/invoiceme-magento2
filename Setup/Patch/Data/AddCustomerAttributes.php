<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;

class AddCustomerAttributes implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $customerAttributes = [
            'msts_im_business_name' => [
                'type'      => 'varchar',
                'label'     => 'Business Name',
                'input'     => 'text',
                'visible'   => false,
                'position'  => 250,
                'required'  => false,
                'system'    => false,
            ],
            'msts_im_status' => [
                'type'      => 'int',
                'label'     => 'Status',
                'input'     => 'select',
                'visible'   => false,
                'source'    => Table::class,
                'position'  => 251,
                'required'  => false,
                'system'    => false,
                'option'    => [
                    'values' => [
                        BuyerStatusInterface::ACTIVE,
                        BuyerStatusInterface::CANCELLED,
                        BuyerStatusInterface::DECLINED,
                        BuyerStatusInterface::INACTIVE,
                        BuyerStatusInterface::PENDING,
                        BuyerStatusInterface::PENDING_DIRECT_DEBIT,
                        BuyerStatusInterface::PENDING_SETUP,
                        BuyerStatusInterface::SUSPENDED,
                        BuyerStatusInterface::WITHDRAWN,
                    ],
                ],
            ],
            'msts_im_currency' => [
                'type'      => 'varchar',
                'label'     => 'Currency',
                'input'     => 'text',
                'visible'   => false,
                'position'  => 252,
                'required'  => false,
                'system'    => false,
            ],
            'msts_im_credit_approved' => [
                'type'      => 'decimal',
                'label'     => 'Approved Limit',
                'input'     => 'text',
                'visible'   => false,
                'position'  => 253,
                'required'  => false,
                'system'    => false,
            ],
            'msts_im_credit_balance' => [
                'type'      => 'decimal',
                'label'     => 'Balance',
                'input'     => 'text',
                'visible'   => false,
                'position'  => 254,
                'required'  => false,
                'system'    => false,
            ],
            'msts_im_credit_preauthorized' => [
                'type'      => 'decimal',
                'label'     => 'Preauthorized',
                'input'     => 'text',
                'visible'   => false,
                'position'  => 255,
                'required'  => false,
                'system'    => false,
            ],
            'msts_im_buyer_id' => [
                'type'      => 'varchar',
                'label'     => 'Buyer ID',
                'input'     => 'hidden',
                'visible'   => false,
                'position'  => 256,
                'required'  => false,
                'system'    => false,
            ],
            'msts_im_client_reference_id' => [
                'type'      => 'static',
                'label'     => 'Client Reference ID',
                'input'     => 'text',
                'visible'   => false,
                'position'  => 257,
                'required'  => false,
                'system'    => false,
            ],
        ];

        foreach ($customerAttributes as $attributeCode => $attributeConfig) {
            $customerSetup->addAttribute(Customer::ENTITY, $attributeCode, $attributeConfig);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function revert()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $customerAttributes = [
            'msts_im_business_name',
            'msts_im_status',
            'msts_im_currency',
            'msts_im_credit_approved',
            'msts_im_credit_balance',
            'msts_im_credit_preauthorized',
            'msts_im_buyer_id',
            'msts_im_client_reference_id',
        ];

        foreach ($customerAttributes as $attributeCode) {
            $customerSetup->removeAttribute(Customer::ENTITY, $attributeCode);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
