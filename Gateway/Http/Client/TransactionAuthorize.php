<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Gateway\Http\Client;

use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Model\Method\Logger;
use Msts\CaaS\Api\Data\Preauthorization\CreateMethod\CreateAPreauthorizationRequestInterface;
use Msts\CaaS\Api\Data\Preauthorization\CreateMethod\CreateAPreauthorizationRequestInterfaceFactory;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\ConfigProvider;
use Psr\Log\LoggerInterface;

class TransactionAuthorize extends AbstractTransaction
{
    /**
     * @var CreateAPreauthorizationRequestInterfaceFactory
     */
    private $preAuthorizationRequestFactory;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    /**
     * @param LoggerInterface $logger
     * @param Logger $paymentLogger
     * @param CreateAPreauthorizationRequestInterfaceFactory $preAuthorizationRequestFactory
     * @param ConfigProvider $configProvider
     * @param CaaSFactory $caaSFactory
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $paymentLogger,
        CreateAPreauthorizationRequestInterfaceFactory $preAuthorizationRequestFactory,
        ConfigProvider $configProvider,
        CaaSFactory $caaSFactory
    ) {
        $this->preAuthorizationRequestFactory = $preAuthorizationRequestFactory;
        $this->configProvider = $configProvider;
        $this->caaSFactory = $caaSFactory;
        parent::__construct($logger, $paymentLogger);
    }

    /**
     * @param array $data
     * @return array
     * @throws ClientException
     */
    protected function process(array $data): array
    {
        /** @var CreateAPreauthorizationRequestInterface $preAuthorizationRequest */
        $preAuthorizationRequest = $this->preAuthorizationRequestFactory->create(['data' => $data]);

        try {
            $caaS = $this->caaSFactory->create();
            $preAuthorizationResponse = $caaS->preauthorization->create(
                $preAuthorizationRequest->getRequestData()
            );
        } catch (ApiClientException $exception) {
            $errorResponse = $exception->getErrorResponse();
            $message = __($exception->getMessage());
            if ($exception->getCode() == 402) {
                $programUrl = $this->configProvider->getProgramUrl();
                $message = __(
                    'Hold on! You currently have insufficient credit, for this purchase. No worries, please visit %1 to request an increase to your credit line.',
                    $programUrl
                );
            } elseif ($exception->getCode() == 400) {
                $apiErrorCode = $errorResponse ? $errorResponse->getCode() : null;
                if ($apiErrorCode === 'preauthorization_po_required') {
                    $message = __('Purchase Order number is required');
                } elseif ($apiErrorCode === 'preauthorization_invalid_po') {
                    $message = __('Purchase Order number is invalid or does not match expected format');
                }
            }

            throw new ClientException($message, $exception);
        }

        return $preAuthorizationResponse->getRequestData();
    }
}
