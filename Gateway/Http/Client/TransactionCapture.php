<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Gateway\Http\Client;

use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use Msts\CaaS\Api\Data\Charge\CreateMethod\CreateAChargeRequestInterface;
use Msts\CaaS\Api\Data\Charge\CreateMethod\CreateAChargeRequestInterfaceFactory;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Api\Data\Charge\ResponseStatusInterface;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Registry\PaymentCapture;
use Psr\Log\LoggerInterface;

class TransactionCapture extends AbstractTransaction
{
    /**
     * @var PaymentCapture
     */
    private $paymentCapture;

    /**
     * @var CreateAChargeRequestInterfaceFactory
     */
    private $createAChargeRequestFactory;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    /**
     * @param LoggerInterface $logger
     * @param Logger $paymentLogger
     * @param PaymentCapture $paymentCapture
     * @param CreateAChargeRequestInterfaceFactory $createAChargeRequestFactory
     * @param ConfigProvider $configProvider
     * @param CaaSFactory $caaSFactory
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $paymentLogger,
        PaymentCapture $paymentCapture,
        CreateAChargeRequestInterfaceFactory $createAChargeRequestFactory,
        ConfigProvider $configProvider,
        CaaSFactory $caaSFactory
    ) {
        $this->paymentCapture = $paymentCapture;
        $this->createAChargeRequestFactory = $createAChargeRequestFactory;
        $this->configProvider = $configProvider;
        $this->caaSFactory = $caaSFactory;
        parent::__construct($logger, $paymentLogger);
    }

    public function placeRequest(TransferInterface $transferObject): array
    {
        if ($this->paymentCapture->isSkipped()) {
            return [];
        }

        return parent::placeRequest($transferObject);
    }

    /**
     * @param array $data
     * @return array
     * @throws ClientException
     */
    protected function process(array $data): array
    {
        /** @var CreateAChargeRequestInterface $createAChargeRequest */
        $createAChargeRequest = $this->createAChargeRequestFactory->create(['data' => $data]);

        try {
            $caaSFactory = $this->caaSFactory->create();
            $chargeResponse = $caaSFactory->charge->create($createAChargeRequest->getRequestData());
        } catch (ApiClientException $exception) {
            $errorResponse = $exception->getErrorResponse();
            $message = __($exception->getMessage());
            if ($exception->getCode() == 402) {
                $programUrl = $this->configProvider->getProgramUrl();
                $message = __(
                    'Hold on! You currently have insufficient credit, for this purchase. No worries, please visit %1 to request an increase to your credit line.',
                    $programUrl
                );
            } elseif ($exception->getCode() == 400) {
                $apiErrorCode = $errorResponse ? $errorResponse->getCode() : null;
                if ($apiErrorCode === 'po_required') {
                    $message = __('Purchase Order number is required');
                } elseif ($apiErrorCode === 'invalid_po') {
                    $message = __('Purchase Order number is invalid or does not match expected format');
                }
            }

            throw new ClientException($message, $exception);
        }

        if ($chargeResponse->getStatus() !== ResponseStatusInterface::CREATED) {
            throw new ClientException(__('Payment capturing error.'));
        }

        return $chargeResponse->getRequestData();
    }
}
