<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Gateway\Http\Client;

use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Model\Method\Logger;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Model\Preauthorization\PreauthorizationApiCall;
use Msts\InvoiceMe\Api\Data\Preauthorization\PreauthorizationStatusInterface;
use Psr\Log\LoggerInterface;

class TransactionVoid extends AbstractTransaction
{
    /**
     * @var PreauthorizationApiCall
     */
    private $preauthorizationApiCall;

    public function __construct(
        LoggerInterface $logger,
        Logger $paymentLogger,
        PreauthorizationApiCall $preauthorizationApiCall
    ) {
        $this->preauthorizationApiCall = $preauthorizationApiCall;
        parent::__construct($logger, $paymentLogger);
    }

    /**
     * @param array $data
     * @return array
     * @throws ClientException
     * @throws ApiClientException
     */
    protected function process(array $data): array
    {
        $preAuthorizationResponse = $this->preauthorizationApiCall->cancel($data['txn_id']);

        if (!$preAuthorizationResponse->getStatus()) {
            throw new ClientException(
                __('Payment voiding error.')
            );
        }

        if ($preAuthorizationResponse->getStatus() !== PreauthorizationStatusInterface::CANCELLED) {
            throw new ClientException(
                __('Payment voiding error. Received status %1', $preAuthorizationResponse->getStatus())
            );
        }

        return $preAuthorizationResponse->getRequestData();
    }
}
