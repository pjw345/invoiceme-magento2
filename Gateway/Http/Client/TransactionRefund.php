<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Gateway\Http\Client;

use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Model\Method\Logger;
use Msts\CaaS\Api\Data\Charge\CancelMethod\CancelAChargeRequestInterface;
use Msts\CaaS\Api\Data\Charge\CancelMethod\CancelAChargeRequestInterfaceFactory;
use Msts\CaaS\Api\Data\Charge\ReturnMethod\ReturnAChargeRequestInterface;
use Msts\CaaS\Api\Data\Charge\ReturnMethod\ReturnAChargeRequestInterfaceFactory;
use Msts\CaaS\Exception\ApiClientException;
use Msts\InvoiceMe\Api\Data\Charge\ResponseStatusInterface;
use Msts\InvoiceMe\Api\Data\Charge\ReturnReasonInterface;
use Msts\InvoiceMe\Model\CaaSFactory;
use Psr\Log\LoggerInterface;

class TransactionRefund extends AbstractTransaction
{
    /**
     * @var CancelAChargeRequestInterfaceFactory
     */
    private $cancelAChargeRequestFactory;

    /**
     * @var ReturnAChargeRequestInterfaceFactory
     */
    private $returnAChargeRequestFactory;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    public function __construct(
        LoggerInterface $logger,
        Logger $paymentLogger,
        CancelAChargeRequestInterfaceFactory $cancelAChargeRequestFactory,
        ReturnAChargeRequestInterfaceFactory $returnAChargeRequestFactory,
        CaaSFactory $caaSFactory
    ) {
        $this->cancelAChargeRequestFactory = $cancelAChargeRequestFactory;
        $this->returnAChargeRequestFactory = $returnAChargeRequestFactory;
        $this->caaSFactory = $caaSFactory;
        parent::__construct($logger, $paymentLogger);
    }

    /**
     * @param array $data
     * @return array
     * @throws ApiClientException
     * @throws ClientException
     */
    protected function process(array $data): array
    {
        if ($data['is_full_refund']) {
            return $this->processCancelACharge($data);
        } else {
            return $this->processReturnACharge($data);
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws ApiClientException
     * @throws ClientException
     */
    private function processCancelACharge(array $data): array
    {
        /** @var CancelAChargeRequestInterface $cancelAChargeRequest */
        $cancelAChargeRequest = $this->cancelAChargeRequestFactory->create(['data' => $data]);
        $cancelAChargeRequest->setReason(ReturnReasonInterface::OTHER);
        $caaS = $this->caaSFactory->create();
        $processCancelACharge = $caaS->charge->cancel($cancelAChargeRequest->getRequestData());
        if ($processCancelACharge->getStatus() !== ResponseStatusInterface::CANCELLED) {
            throw new ClientException(__('Payment refund error.'));
        }

        return $processCancelACharge->getRequestData();
    }

    /**
     * @param array $data
     * @return array
     * @throws ApiClientException
     * @throws ClientException
     */
    private function processReturnACharge(array $data): array
    {
        /** @var ReturnAChargeRequestInterface $returnAChargeRequest */
        $returnAChargeRequest = $this->returnAChargeRequestFactory->create();
        $returnAChargeRequest->setId($data['id']);
        $returnAChargeRequest->setReturnAmount($data['return_amount']);
        $returnAChargeRequest->setTotalAmount($data['total_amount']);
        $returnAChargeRequest->setTaxAmount($data['tax_amount']);
        $returnAChargeRequest->setDiscountAmount($data['discount_amount']);
        $returnAChargeRequest->setShippingAmount($data['shipping_amount']);
        $returnAChargeRequest->setShippingTaxAmount($data['shipping_tax_amount']);

        if ($data['shipping_tax_details'] !== null && !empty($data['shipping_tax_details'])) {
            $returnAChargeRequest->setShippingTaxDetails($data['shipping_tax_details']);
        }

        $returnAChargeRequest->setShippingDiscountAmount($data['shipping_discount_amount']);

        $returnAChargeRequest->setDetails($data['details']);
        $returnAChargeRequest->setReturnReason($data['return_reason']);

        $caaS = $this->caaSFactory->create();
        $processReturnACharge = $caaS->charge->return($returnAChargeRequest->getRequestData());

        if ($processReturnACharge->getStatus() !== ResponseStatusInterface::CREATED) {
            throw new ClientException(__('Payment refund error.'));
        }

        return $processReturnACharge->getRequestData();
    }
}
