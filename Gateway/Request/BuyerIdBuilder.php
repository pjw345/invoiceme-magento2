<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Gateway\Request;

use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Msts\InvoiceMe\Model\GenerateGenericMessage;
use Msts\InvoiceMe\Registry\PaymentCapture;

class BuyerIdBuilder extends AbstractBuilder
{
    private const BUYER_ID = 'buyer_id';

    /**
     * @var PaymentCapture
     */
    private $paymentCapture;

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var CustomerRegistry
     */
    private $customerRegistry;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var GenerateGenericMessage
     */
    private $generateGenericMessage;

    public function __construct(
        PaymentCapture $paymentCapture,
        SubjectReader $subjectReader,
        CustomerRegistry $customerRegistry,
        EncryptorInterface $encryptor,
        GenerateGenericMessage $genericMessageGenerator
    ) {
        $this->paymentCapture = $paymentCapture;
        $this->subjectReader = $subjectReader;
        $this->customerRegistry = $customerRegistry;
        $this->encryptor = $encryptor;
        $this->generateGenericMessage = $genericMessageGenerator;

        parent::__construct($subjectReader);
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     * @throws LocalizedException
     */
    public function build(array $buildSubject): array
    {
        if ($this->paymentCapture->isSkipped()) {
            return [];
        }

        parent::build($buildSubject);
        $paymentDO = $this->subjectReader->readPayment($buildSubject);

        $order = $paymentDO->getOrder();
        $customer = $this->customerRegistry->retrieve($order->getCustomerId());
        $buyerId = $this->encryptor->decrypt($customer->getData('msts_im_buyer_id'));
        if (!$buyerId) {
            throw new LocalizedException($this->generateGenericMessage->execute());
        }

        return [
            self::BUYER_ID => $buyerId,
        ];
    }
}
