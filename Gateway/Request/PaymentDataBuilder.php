<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Gateway\Request;

use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Msts\InvoiceMe\Model\CurrencyConverter;

class PaymentDataBuilder extends AbstractBuilder
{
    private const CURRENCY = 'currency';

    private const PREAUTHORIZED_AMOUNT = 'preauthorized_amount';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var CustomerRegistry
     */
    private $customerRegistry;

    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;

    public function __construct(
        SubjectReader $subjectReader,
        CustomerRegistry $customerRegistry,
        CurrencyConverter $currencyConverter
    ) {
        $this->subjectReader = $subjectReader;
        $this->customerRegistry = $customerRegistry;
        $this->currencyConverter = $currencyConverter;
        parent::__construct($subjectReader);
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     * @throws LocalizedException
     */
    public function build(array $buildSubject): array
    {
        parent::build($buildSubject);
        $paymentDO = $this->subjectReader->readPayment($buildSubject);

        $order = $paymentDO->getOrder();
        $customer = $this->customerRegistry->retrieve($order->getCustomerId());
        $multiplier = $this->currencyConverter->getMultiplier($customer->getData('msts_im_currency'));

        return [
            self::CURRENCY => $order->getCurrencyCode(),
            self::PREAUTHORIZED_AMOUNT => (int)round($order->getGrandTotalAmount() * $multiplier),
        ];
    }
}
