<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Controller\Gateway;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Multishipping\Model\Checkout\Type\Multishipping;
use Msts\InvoiceMe\Model\IsModuleFullyConfigured;
use Msts\InvoiceMe\Model\Order\UpdateOrdersBeforeGatewayRedirect;

class MultishippingRedirect extends Action implements HttpGetActionInterface
{
    /**
     * @var IsModuleFullyConfigured
     */
    private $isModuleFullyConfigured;

    /**
     * @var Multishipping
     */
    private $multishipping;

    /**
     * @var UpdateOrdersBeforeGatewayRedirect
     */
    private $updateOrdersBeforeGatewayRedirect;

    public function __construct(
        Context $context,
        IsModuleFullyConfigured $isModuleFullyConfigured,
        Multishipping $multishipping,
        UpdateOrdersBeforeGatewayRedirect $updateOrdersBeforeGatewayRedirect
    ) {
        parent::__construct($context);

        $this->isModuleFullyConfigured = $isModuleFullyConfigured;
        $this->multishipping = $multishipping;
        $this->updateOrdersBeforeGatewayRedirect = $updateOrdersBeforeGatewayRedirect;
    }

    /**
     * @param RequestInterface $request
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->isModuleFullyConfigured->execute()) {
            $this->_forward('noroute');
            $this->getActionFlag()->set('', self::FLAG_NO_DISPATCH, true);

            return $this->getResponse();
        }

        return parent::dispatch($request);
    }

    /**
     * Updates orders and redirects customer to InvoiceMe credit application form
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $this->updateOrdersBeforeGatewayRedirect->execute($this->multishipping->getOrderIds());
        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/customer/applyForCredit');
    }
}
