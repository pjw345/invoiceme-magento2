<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Controller\Customer;

use Exception;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page;
use Magento\Store\Model\StoreManagerInterface;
use Msts\InvoiceMe\Api\Data\Buyer\BuyerStatusInterface;
use Msts\InvoiceMe\Model\Cart\GetCustomerCurrentTransactionAmount;
use Msts\InvoiceMe\Model\ConfigProvider;
use Msts\InvoiceMe\Model\Customer\AssignUniqueClientReferenceIdToCustomer;
use Msts\InvoiceMe\Model\Customer\GetCustomerStatus;
use Msts\InvoiceMe\Model\Order\GetCustomerFirstTransactedDate;
use Msts\InvoiceMe\Model\Order\GetCustomerTransactedTotal;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ApplyForCredit extends Action implements HttpGetActionInterface
{
    /**
     * Customers with buyer status set to the following will be allowed to access the URL:
     * /msts_invoiceme/customer/applyForCredit/
     */
    private const VALID_BUYER_STATUSES_TO_REAPPLY_FOR_CREDIT = [
        BuyerStatusInterface::CANCELLED,
        BuyerStatusInterface::WITHDRAWN,
    ];

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var LoggerInterface
     */
    private $invoiceMeLogger;

    /**
     * @var GetCustomerFirstTransactedDate
     */
    private $getCustomerFirstTransactedDate;

    /**
     * @var GetCustomerTransactedTotal
     */
    private $getCustomerTransactedTotal;

    /**
     * @var GetCustomerCurrentTransactionAmount
     */
    private $getCustomerCurrentTransactionAmount;

    /**
     * @var AssignUniqueClientReferenceIdToCustomer
     */
    private $assignUniqueClientReferenceIdToCustomer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var GetCustomerStatus
     */
    private $getCustomerStatus;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param ConfigProvider $configProvider
     * @param LoggerInterface $invoiceMeLogger
     * @param GetCustomerFirstTransactedDate $getCustomerFirstTransactedDate
     * @param GetCustomerTransactedTotal $getCustomerTransactedTotal
     * @param GetCustomerCurrentTransactionAmount $getCustomerCurrentTransactionAmount
     * @param AssignUniqueClientReferenceIdToCustomer $assignUniqueClientReferenceIdToCustomer
     * @param LoggerInterface $logger
     * @param GetCustomerStatus $getCustomerStatus
     * @param StoreManagerInterface $storeManager
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        ConfigProvider $configProvider,
        LoggerInterface $invoiceMeLogger,
        GetCustomerFirstTransactedDate $getCustomerFirstTransactedDate,
        GetCustomerTransactedTotal $getCustomerTransactedTotal,
        GetCustomerCurrentTransactionAmount $getCustomerCurrentTransactionAmount,
        AssignUniqueClientReferenceIdToCustomer $assignUniqueClientReferenceIdToCustomer,
        LoggerInterface $logger,
        GetCustomerStatus $getCustomerStatus,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->configProvider = $configProvider;
        $this->invoiceMeLogger = $invoiceMeLogger;
        $this->getCustomerFirstTransactedDate = $getCustomerFirstTransactedDate;
        $this->getCustomerTransactedTotal = $getCustomerTransactedTotal;
        $this->getCustomerCurrentTransactionAmount = $getCustomerCurrentTransactionAmount;
        $this->assignUniqueClientReferenceIdToCustomer = $assignUniqueClientReferenceIdToCustomer;
        $this->logger = $logger;
        $this->getCustomerStatus = $getCustomerStatus;
        $this->storeManager = $storeManager;
    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if (!$this->isBuyerValidToApplyForCredit()) {
                return $resultRedirect->setPath('*/*');
            }

            $applyForCreditUrl = $this->buildApplyForCreditUrl('*/*', '*/*');
        } catch (LocalizedException | NoSuchEntityException $e) {
            $this->logger->critical('Error during generating credit URL', ['exception' => $e]);
            $this->messageManager->addErrorMessage(__('There was an error trying to apply for a credit.'));

            return $resultRedirect->setPath('*/*');
        }
        if ($applyForCreditUrl) {
            return $this->_redirect($applyForCreditUrl);
        }

        return $resultRedirect->setPath('*/*');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    private function isBuyerValidToApplyForCredit(): bool
    {
        $buyerStatus = $this->getCustomerStatus->execute($this->customerSession->getCustomer());

        return !$buyerStatus || in_array($buyerStatus, self::VALID_BUYER_STATUSES_TO_REAPPLY_FOR_CREDIT);
    }

    /**
     * Build request URL to InvoiceMe credit application form
     *
     * @param string $failurePath
     * @param string $successPath
     * @return string|null
     * @throws NoSuchEntityException
     */
    private function buildApplyForCreditUrl(string $failurePath, string $successPath): ?string
    {
        $customer = $this->customerSession->getCustomer();

        $programUrl = $this->configProvider->getProgramUrl();
        if (!$programUrl) {
            $this->messageManager->addErrorMessage(__('There was an error trying to apply for a credit.'));
            $debugData = [
                'module_validation_error' => 'Program URL cannot be empty.',
            ];
            $this->invoiceMeLogger->debug('Building Apply for a credit URL', $debugData);
            $this->_redirect($failurePath);

            return null;
        }

        $clientReferenceId = $customer->getMstsImClientReferenceId();
        if (!$clientReferenceId) {
            try {
                $clientReferenceId = $this->assignUniqueClientReferenceIdToCustomer->execute($customer);
            } catch (Exception $e) {
                $debugData = [
                    'exception' => ['error' => $e->getMessage(), 'code' => $e->getCode()],
                ];
                $this->invoiceMeLogger->debug('Building Apply for a credit URL', $debugData);
                $this->messageManager->addErrorMessage(__('There was an error trying to apply for a credit.'));
                $this->_redirect($failurePath);

                return null;
            }
        }

        $applyForCreditUrl = $programUrl . 'apply'
            . '?client_reference_id=' . $clientReferenceId
            . '&ecommerce_url=' . $this->_url->getUrl($successPath, ['_secure' => true]);

        $customerFirstTransactedDate = $this->getCustomerFirstTransactedDate->execute($customer);
        $includeTransactionCurrency = false;
        if ($customerFirstTransactedDate) {
            $applyForCreditUrl .= '&first_transacted_date=' . $customerFirstTransactedDate;
            $customerTransactedTotal = $this->getCustomerTransactedTotal->execute($customer);
            if ($customerTransactedTotal) {
                $applyForCreditUrl .= '&total_transacted_amount=' . $customerTransactedTotal;
                $includeTransactionCurrency = true;
            }
        }

        $customerCurrentTransactionAmount = $this->getCustomerCurrentTransactionAmount->execute();
        if ($customerCurrentTransactionAmount) {
            $applyForCreditUrl .= '&current_transaction_amount=' . $customerCurrentTransactionAmount;
            $includeTransactionCurrency = true;
        }

        if ($includeTransactionCurrency) {
            $applyForCreditUrl .= '&transaction_currency=' . $this->storeManager->getStore()->getBaseCurrencyCode();
        }

        return $applyForCreditUrl;
    }
}
