<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Controller\Adminhtml\Config;

use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Serialize\Serializer\Json;
use Msts\CaaS\Model\MaskValue;
use Msts\CaaS\Model\Webhook\WebhookApiCall;
use Msts\InvoiceMe\Model\CaaSFactory;
use Msts\InvoiceMe\Model\Webhook\CheckWebhooksStatus;
use Msts\InvoiceMe\Model\Webhook\Config\UpdateCreatedWebhooksConfig;
use Msts\InvoiceMe\Model\Webhook\DeleteAllWebhooks;
use Msts\InvoiceMe\Model\Webhook\ValidateApiKeyForCreatedWebhooks;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CheckCreatedWebhooks extends Action implements HttpGetActionInterface
{
    /**
     * Authorization resource
     */
    public const ADMIN_RESOURCE = 'Msts_InvoiceMe::invoiceme';

    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    /**
     * @var CheckWebhooksStatus
     */
    private $checkWebhooksStatus;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ValidateApiKeyForCreatedWebhooks
     */
    private $validateApiKeyForCreatedWebhooks;

    /**
     * @var DeleteAllWebhooks
     */
    private $deleteAllWebhooks;

    /**
     * @var ReinitableConfigInterface
     */
    private $reinitableConfig;

    /**
     * @var UpdateCreatedWebhooksConfig
     */
    private $updateCreatedWebhooksConfig;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var MaskValue
     */
    private $maskValue;

    /**
     * @var CaaSFactory
     */
    private $caaSFactory;

    /**
     * @param Action\Context $context
     * @param JsonFactory $jsonResultFactory
     * @param CheckWebhooksStatus $checkWebhooksStatus
     * @param LoggerInterface $logger
     * @param ValidateApiKeyForCreatedWebhooks $validateApiKeyForCreatedWebhooks
     * @param DeleteAllWebhooks $deleteAllWebhooks
     * @param ReinitableConfigInterface $reinitableConfig
     * @param UpdateCreatedWebhooksConfig $updateCreatedWebhooksConfig
     * @param Json $serializer
     * @param MaskValue $maskValue
     * @param CaaSFactory $caaSFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Action\Context $context,
        JsonFactory $jsonResultFactory,
        CheckWebhooksStatus $checkWebhooksStatus,
        LoggerInterface $logger,
        ValidateApiKeyForCreatedWebhooks $validateApiKeyForCreatedWebhooks,
        DeleteAllWebhooks $deleteAllWebhooks,
        ReinitableConfigInterface $reinitableConfig,
        UpdateCreatedWebhooksConfig $updateCreatedWebhooksConfig,
        Json $serializer,
        MaskValue $maskValue,
        CaaSFactory $caaSFactory
    ) {
        parent::__construct($context);
        $this->jsonResultFactory = $jsonResultFactory;
        $this->checkWebhooksStatus = $checkWebhooksStatus;
        $this->logger = $logger;
        $this->validateApiKeyForCreatedWebhooks = $validateApiKeyForCreatedWebhooks;
        $this->deleteAllWebhooks = $deleteAllWebhooks;
        $this->reinitableConfig = $reinitableConfig;
        $this->updateCreatedWebhooksConfig = $updateCreatedWebhooksConfig;
        $this->serializer = $serializer;
        $this->maskValue = $maskValue;
        $this->caaSFactory = $caaSFactory;
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Json|ResultInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        if (!$this->getRequest()->isAjax()) {
            throw new NotFoundException(__('Page not found.'));
        }

        $scope = $this->getRequest()->getParam('scope', 'default');
        $scopeId = (int)$this->getRequest()->getParam('scopeId');

        $result = [];
        try {
            $caaS = $this->caaSFactory->create([], $scope, $scopeId);
            if (!$this->validateApiKeyForCreatedWebhooks->execute($scope, $scopeId, true)) {
                $this->deleteAllWebhooks->execute($scope, $scopeId);
            }
            $webhooks = $caaS->webhooks->list();
            $this->updateCreatedWebhooksConfig->execute($webhooks, $scope, $scopeId);
            $this->reinitableConfig->reinit();
            $result = $this->checkWebhooksStatus->execute($scope, $scopeId);
            $result['status'] = 'success';
            $result['createdWebhooks'] = $this->maskValue->maskValues(
                $this->serializer->unserialize($this->serializer->serialize($webhooks)),
                WebhookApiCall::METHOD_NAME
            );
        } catch (Exception $exception) {
            $this->logger->error($exception);
            $result = $this->updateResponseWithErrorMessage($exception, $result);
        }
        $jsonResult = $this->jsonResultFactory->create();
        $jsonResult->setData($result);

        return $jsonResult;
    }

    /**
     * @param Exception $exception
     * @param array $result
     * @return array
     */
    private function updateResponseWithErrorMessage(Exception $exception, array $result): array
    {
        $result['status'] = 'error';
        if ($exception instanceof LocalizedException) {
            $result['message'] = __(
                'An error occurred with the following message: "%1". Please check the exception log for more details.',
                $exception->getMessage()
            );
        } else {
            $result['message'] = __(
                'An error occurred with the following message: "%1". Please enable the debug mode, try again and check the debug log for more details.',
                $exception->getMessage()
            );
        }

        return $result;
    }
}
