<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Controller\Webhook;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Msts\InvoiceMe\Api\Data\Webhook\EventTypeInterface;
use Msts\InvoiceMe\Controller\Webhook;
use Msts\InvoiceMe\Exception\Webhook\BuyerStatus\InvalidStatusException;
use Msts\InvoiceMe\Exception\Webhook\SchemaValidationException;
use Msts\InvoiceMe\Model\IsModuleFullyConfigured;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\BuyerStatus\ProcessWebhook;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\PrepareDebugData;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\ValidateAuthorizationHeader;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\ValidateWebhookAuthTokenForCreatedWebhooks;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class BuyerStatus extends Webhook implements HttpPostActionInterface
{
    /**
     * @var PrepareDebugData
     */
    private $prepareDebugData;

    /**
     * @var ProcessWebhook
     */
    private $processWebhook;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Context $context,
        IsModuleFullyConfigured $isModuleFullyConfigured,
        ValidateWebhookAuthTokenForCreatedWebhooks $validateWebhookAuthTokenForCreatedWebhooks,
        ValidateAuthorizationHeader $validateAuthorizationHeader,
        Json $jsonSerializer,
        LoggerInterface $invoiceMeLogger,
        PrepareDebugData $prepareDebugData,
        ProcessWebhook $processWebhook,
        LoggerInterface $logger
    ) {
        $this->prepareDebugData = $prepareDebugData;
        $this->processWebhook = $processWebhook;
        $this->logger = $logger;
        parent::__construct(
            $context,
            $isModuleFullyConfigured,
            $validateWebhookAuthTokenForCreatedWebhooks,
            $validateAuthorizationHeader,
            $jsonSerializer,
            $invoiceMeLogger
        );
    }

    /**
     * 'buyer.status' webhook call from InvoiceMe
     *
     * {@inheritdoc}
     */
    public function execute()
    {
        $request = $this->getRequest();
        $inputData = $this->jsonSerializer->unserialize($request->getContent());
        $debugData = $this->prepareDebugData->execute($request, EventTypeInterface::BUYER_STATUS, true);

        try {
            $this->processWebhook->execute($inputData);
        } catch (SchemaValidationException | InvalidStatusException $e) {
            $this->setErrorResponse($e->getMessage());
            $this->logDebugData($debugData);

            return $this->getResponse();
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            $this->setSuccessResponse(
                sprintf(
                    "Customer was not found for the specified 'client_reference_id' = %s",
                    $inputData['data']['client_reference_id']
                )
            );
            $this->logDebugData($debugData);

            return $this->getResponse();
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            $this->setErrorResponse(
                sprintf(
                    "There was an error trying to update customer's data. The error message: %s",
                    $e->getMessage()
                )
            );
            $this->logDebugData($debugData);

            return $this->getResponse();
        }

        $this->setSuccessResponse(sprintf("Customer's data have been successfully updated"));
        $this->logDebugData($debugData);

        return $this->getResponse();
    }
}
