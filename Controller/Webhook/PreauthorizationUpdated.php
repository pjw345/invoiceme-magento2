<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Controller\Webhook;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Msts\InvoiceMe\Api\Data\Webhook\EventTypeInterface;
use Msts\InvoiceMe\Controller\Webhook;
use Msts\InvoiceMe\Exception\Webhook\SchemaValidationException;
use Msts\InvoiceMe\Model\IsModuleFullyConfigured;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\PreauthorizationUpdated\ProcessWebhook;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\PrepareDebugData;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\ValidateAuthorizationHeader;
use Msts\InvoiceMe\Model\Webhook\IncomingRequest\ValidateWebhookAuthTokenForCreatedWebhooks;
use Psr\Log\LoggerInterface;

class PreauthorizationUpdated extends Webhook implements HttpPostActionInterface
{
    /**
     * @var PrepareDebugData
     */
    private $prepareDebugData;

    /**
     * @var ProcessWebhook
     */
    private $processWebhook;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Context $context,
        IsModuleFullyConfigured $isModuleFullyConfigured,
        ValidateWebhookAuthTokenForCreatedWebhooks $validateWebhookAuthTokenForCreatedWebhooks,
        ValidateAuthorizationHeader $validateAuthorizationHeader,
        Json $jsonSerializer,
        LoggerInterface $invoiceMeLogger,
        PrepareDebugData $prepareDebugData,
        ProcessWebhook $processWebhook,
        LoggerInterface $logger
    ) {
        $this->prepareDebugData = $prepareDebugData;
        $this->processWebhook = $processWebhook;
        $this->logger = $logger;
        parent::__construct(
            $context,
            $isModuleFullyConfigured,
            $validateWebhookAuthTokenForCreatedWebhooks,
            $validateAuthorizationHeader,
            $jsonSerializer,
            $invoiceMeLogger
        );
    }

    /**
     * 'preauthorization.updated' webhook call from InvoiceMe
     *
     * {@inheritdoc}
     */
    public function execute()
    {
        $request = $this->getRequest();
        $inputData = $this->jsonSerializer->unserialize($request->getContent());
        $debugData = $this->prepareDebugData->execute($request, EventTypeInterface::PRE_AUTHORIZATION_UPDATED);

        try {
            $this->processWebhook->execute($inputData);
        } catch (SchemaValidationException $e) {
            $this->setErrorResponse($e->getMessage());
            $this->logDebugData($debugData);

            return $this->getResponse();
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            $this->setSuccessResponse(
                sprintf(
                    "Transaction was not found for the specified 'id' = %s",
                    $inputData['data']['id']
                )
            );
            $this->logDebugData($debugData);

            return $this->getResponse();
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            $this->setErrorResponse(
                sprintf(
                    "There was an error trying to update transaction's data. The error message: %s",
                    $e->getMessage()
                )
            );
            $this->logDebugData($debugData);

            return $this->getResponse();
        }

        $this->setSuccessResponse(sprintf("Transaction's data have been successfully updated"));
        $this->logDebugData($debugData);

        return $this->getResponse();
    }
}
