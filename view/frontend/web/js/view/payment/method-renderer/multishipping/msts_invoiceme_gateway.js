/*browser:true*/
define([
    'ko',
    'jquery',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote',
    'Msts_InvoiceMe/js/model/msts_invoiceme_maxlength'
], function (
    ko,
    $,
    PriceUtils,
    Component,
    customer,
    quote,
    maxLengthModel
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Msts_InvoiceMe/payment/multishipping/form',
            invoicemePoNumber: '',
            invoicemeNotes: ''
        },

        initialize: function () {
            this._super()
                .observe([
                    'invoicemePoNumber',
                    'invoicemeNotes'
                ]);

            quote.paymentMethod.subscribe(this._setMultiShippingStateMstsInvoiceMeOff, this, 'beforeChange');
            quote.paymentMethod.subscribe(this._setMultiShippingStateMstsInvoiceMeOn, this, 'change');
        },

        invoiceMeSectionClick: function () {
            window.location.href = window.checkoutConfig.payment.msts_invoiceme.invoiceMeSectionUrl;
        },

        creditCurrencyCode: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_currency')) {
                return;
            }

            return customer.customerData.custom_attributes.msts_im_currency.value;
        },

        creditApprovedLimit: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_credit_approved')) {
                return 0;
            }

            return customer.customerData.custom_attributes.msts_im_credit_approved.value;
        },

        creditBalance: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_credit_balance')) {
                return 0;
            }

            return customer.customerData.custom_attributes.msts_im_credit_balance.value;
        },

        creditPreauthorized: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_credit_preauthorized')) {
                return 0;
            }

            return customer.customerData.custom_attributes.msts_im_credit_preauthorized.value;
        },

        creditAvailable: function () {
            return this.creditApprovedLimit() - this.creditBalance() - this.creditPreauthorized();
        },

        formatPrice: function(value) {
            var priceFormat = Object.assign(
                {},
                window.checkoutConfig.priceFormat,
                { pattern: '%s ' + this.creditCurrencyCode() }
            );

            return PriceUtils.formatPrice(value, priceFormat, false);
        },

        isNotRegisteredBuyer: function () {
            return !(customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_status')
            );
        },

        /* eslint-disable max-len */
        isRegisteredButNotActiveBuyer: function () {
            return customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_status')
                && customer.customerData.custom_attributes.msts_im_status.value !== window.checkoutConfig.payment.msts_invoiceme.buyerStatusActiveOptionId;
        },
        /* eslint-enable max-len */

        getMaxLengthForPurchaserOrderNumber: function () {
            return maxLengthModel.getMaxLength('invoiceme_po_number', 200);
        },

        getMaxLengthForNotes: function () {
            return maxLengthModel.getMaxLength('invoiceme_notes', 1000);
        },

        _setMultiShippingStateMstsInvoiceMeOff: function (previousPaymentMethod) {
            if (previousPaymentMethod
                && previousPaymentMethod.method === this.getCode()) {
                $('#payment-continue').attr('disabled', false);
            }
        },

        _setMultiShippingStateMstsInvoiceMeOn: function (newPaymentMethod) {
            if (newPaymentMethod
                && newPaymentMethod.method === this.getCode()
                && this.isRegisteredButNotActiveBuyer()) {
                $('#payment-continue').attr('disabled', true);
            }
        }
    });
});
