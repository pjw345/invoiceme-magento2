/*browser:true*/
define([
    'jquery',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Customer/js/model/customer',
    'Msts_InvoiceMe/js/model/msts_invoiceme_maxlength'
], function (
    $,
    PriceUtils,
    Component,
    customer,
    maxLengthModel
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Msts_InvoiceMe/payment/form',
            invoicemePoNumber: '',
            invoicemeNotes: ''
        },

        initObservable: function () {
            this._super()
                .observe([
                    'invoicemePoNumber',
                    'invoicemeNotes'
                ]);
            return this;
        },

        getCode: function () {
            return 'msts_invoiceme';
        },

        getData: function () {
            return {
                'method': this.item.method,
                'additional_data': {
                    'invoiceme_po_number': this.invoicemePoNumber(),
                    'invoiceme_notes': this.invoicemeNotes()
                }
            };
        },

        isLoggedIn: function () {
            return customer.isLoggedIn();
        },

        signInClick: function () {
            window.location.href = window.checkout.customerLoginUrl;
        },

        invoiceMeSectionClick: function () {
            window.location.href = window.checkoutConfig.payment.msts_invoiceme.invoiceMeSectionUrl;
        },

        creditCurrencyCode: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_currency')) {
                return;
            }

            return customer.customerData.custom_attributes.msts_im_currency.value;
        },

        creditApprovedLimit: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_credit_approved')) {
                return 0;
            }

            return customer.customerData.custom_attributes.msts_im_credit_approved.value;
        },

        creditBalance: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_credit_balance')) {
                return 0;
            }

            return customer.customerData.custom_attributes.msts_im_credit_balance.value;
        },

        creditPreauthorized: function () {
            if (!customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_credit_preauthorized')) {
                return 0;
            }

            return customer.customerData.custom_attributes.msts_im_credit_preauthorized.value;
        },

        creditAvailable: function () {
            return this.creditApprovedLimit() - this.creditBalance() - this.creditPreauthorized();
        },

        formatPrice: function(value) {
            var priceFormat = Object.assign(
                {},
                window.checkoutConfig.priceFormat,
                { pattern: '%s ' + this.creditCurrencyCode() }
            );

            return PriceUtils.formatPrice(value, priceFormat, false);
        },

        isNotRegisteredBuyer: function () {
            return !(customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_status')
            );
        },

        /* eslint-disable max-len */
        isRegisteredButNotActiveBuyer: function () {
            return customer.customerData.hasOwnProperty('custom_attributes')
                && customer.customerData.custom_attributes.hasOwnProperty('msts_im_status')
                && customer.customerData.custom_attributes.msts_im_status.value !== window.checkoutConfig.payment.msts_invoiceme.buyerStatusActiveOptionId;
        },
        /* eslint-enable max-len */

        afterPlaceOrder: function () {
            if (this.isNotRegisteredBuyer()) {
                this.redirectAfterPlaceOrder = false;
                $.mage.redirect(window.checkoutConfig.payment.msts_invoiceme.redirectUrl);
            }
        },

        getMaxLengthForPurchaserOrderNumber: function () {
            return maxLengthModel.getMaxLength('invoiceme_po_number', 200);
        },

        getMaxLengthForNotes: function () {
            return maxLengthModel.getMaxLength('invoiceme_notes', 1000);
        },

    });
});
