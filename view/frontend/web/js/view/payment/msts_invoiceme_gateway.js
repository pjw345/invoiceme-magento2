/*browser:true*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'msts_invoiceme',
                component: 'Msts_InvoiceMe/js/view/payment/method-renderer/msts_invoiceme_gateway'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    }
);
