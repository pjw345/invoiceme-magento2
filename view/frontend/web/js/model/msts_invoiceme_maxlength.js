define([], function () {
    'use strict';

    var getMaxLength = function (fieldName, defaultMaxLength) {
        var mstsConfig = window.checkoutConfig.payment.msts_invoiceme;

        return mstsConfig[fieldName] && mstsConfig[fieldName].maxlength
            ? mstsConfig[fieldName].maxlength
            : defaultMaxLength;
    };

    return {
        getMaxLength: getMaxLength
    };
});