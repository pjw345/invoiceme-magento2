<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Exception\Webhook;

use Exception;

class SchemaValidationException extends Exception
{
}
