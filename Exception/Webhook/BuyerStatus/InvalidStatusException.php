<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Exception\Webhook\BuyerStatus;

use Exception;

class InvalidStatusException extends Exception
{
}
