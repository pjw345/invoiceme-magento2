<?php
declare(strict_types=1);

namespace Msts\InvoiceMe\Exception;

use Magento\Framework\Exception\LocalizedException;

class NotUniqueClientReferenceIdException extends LocalizedException
{
}
